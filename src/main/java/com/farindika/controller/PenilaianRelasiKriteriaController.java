/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.BerkasDao;
import com.farindika.db.dao.KriteriaDao;
import com.farindika.db.dao.PenilaianRelasiKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class PenilaianRelasiKriteriaController 
{
     private KriteriaDao kriteriaDao;
    private BerkasDao berkasDao;
    private PenilaianRelasiKriteriaDao penilaianRelasiKriteriaDao;
    private List<Kriteria> kriterias;
    private List<Kriteria> kriteriasM;
    private List<Berkas> berkass;
    private List<PenilaianRelasiKriteria> penilaianRelasiKriterias;
    private List<PenilaianRelasiKriteria> penilaianRelasiKriteriasM;
    
    public PenilaianRelasiKriteriaController () 
    {
        kriteriaDao = HibernateUtil.getKriteriaDao();
        berkasDao = HibernateUtil.getBerkasDao();
        penilaianRelasiKriteriaDao = HibernateUtil.getPenilaianRelasiKriteriaDao();
    }
     
    public void save(PenilaianRelasiKriteria penilaianRelasiKriteria) 
    {
        penilaianRelasiKriteriaDao.save(penilaianRelasiKriteria);
    }

    public void update(PenilaianRelasiKriteria penilaianRelasiKriteria) 
    {
        penilaianRelasiKriteriaDao.update(penilaianRelasiKriteria);
    }

    public void delete(PenilaianRelasiKriteria penilaianRelasiKriteria) 
    {
        penilaianRelasiKriteriaDao.delete(penilaianRelasiKriteria);
    }
    
     /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findPenilaianRelasiKriteriaFromTableIndex(tableIndex));
    }

    public List<PenilaianRelasiKriteria> findAll() 
    {
        penilaianRelasiKriterias = penilaianRelasiKriteriaDao.findAll();
        return penilaianRelasiKriterias;
    }
    
    public List<PenilaianRelasiKriteria> findByBerkas(Berkas berkas) 
    {
        penilaianRelasiKriterias = penilaianRelasiKriteriaDao.findByBerkas(berkas);
        return penilaianRelasiKriterias;
    }
    
    public List<PenilaianRelasiKriteria> findByBerkasKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) 
    {
        penilaianRelasiKriteriasM = penilaianRelasiKriteriaDao.findByBerkasKriteriaDipengaruhi(berkas, kriteria);
        return penilaianRelasiKriteriasM;
    }
    
    public List<Kriteria> findByBerkasKriteria(Berkas berkas) 
    {
        kriterias = penilaianRelasiKriteriaDao.findByBerkasKriteria(berkas);
        return kriterias;
    }
    
    public List<Kriteria> findByKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) 
    {
        kriteriasM = penilaianRelasiKriteriaDao.findByKriteriaDipengaruhi(berkas, kriteria);
        return kriteriasM;
    }
    
//    public List<PenilaianRelasiKriteria> findRelasiKriteria(Berkas berkas, Kriteria kriteria1, Kriteria kriteria2)
//    {
//        penilaianRelasiKriteriasM = penilaianRelasiKriteriaDao.findRelasiKriteria(berkas, kriteria1, kriteria2);
//        return penilaianRelasiKriteriasM; 
//    }
    
    
    public PenilaianRelasiKriteria findPenilaianRelasiKriteriaFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || penilaianRelasiKriterias == null) 
        {
            return null;
        }
        return penilaianRelasiKriterias.get(tableIndex);
    }
    
    public PenilaianRelasiKriteria findPenilaianRelasiKriteriaMempengaruhiFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || penilaianRelasiKriteriasM == null) 
        {
            return null;
        }
        return penilaianRelasiKriteriasM.get(tableIndex);
    }

    public List<Kriteria> findKriteriaAll() 
    {
        kriterias = kriteriaDao.findAll();
        return kriterias;
    }
    
      public List<Berkas> findBerkasAll() 
    {
        berkass = berkasDao.findAll();
        return berkass;
    }

    public Object[][] findAllArray() 
    {
        findAll();
        
        Object[][] datas = new Object[penilaianRelasiKriterias == null ? 0 : penilaianRelasiKriterias.size()][4];
        for (int row = 0; penilaianRelasiKriterias != null && row < penilaianRelasiKriterias.size(); row++) 
        {
            datas[row][0] = penilaianRelasiKriterias.get(row).getId();
            datas[row][1] = penilaianRelasiKriterias.get(row).getBerkas().getNama();
            datas[row][2] = penilaianRelasiKriterias.get(row).getKriteriaD().getNama();
            datas[row][3] = penilaianRelasiKriterias.get(row).getKriteriaM().getNama();
            
        }
        return datas;
    }
    
     
     public String[] findKriteriaDipengaruhiAllArray(Berkas berkas) {
        findByBerkas(berkas);
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getNama();
        }
        return datas;
    }
     
     public String[] findNamaKriteriaMempengaruhiAllArray(Berkas berkas, Kriteria kriteria) {
        findByKriteriaDipengaruhi(berkas, kriteria);
        String[] datas = new String[kriteriasM == null ? 0 : kriteriasM.size()];
        for (int i = 0; kriteriasM != null && i < kriteriasM.size(); i++) {
            datas[i] = kriteriasM.get(i).getNama();
        }
        return datas;
    }
     
     public String[] findKriteriaMempengaruhiAllArray(Berkas berkas, Kriteria kriteria) {
        findByKriteriaDipengaruhi(berkas, kriteria);
        String[] datas = new String[kriteriasM == null ? 0 : kriteriasM.size()];
        for (int i = 0; kriteriasM != null && i < kriteriasM.size(); i++) {
            datas[i] = kriteriasM.get(i).getKode();
        }
        return datas;
    }
    
    public String[] findKriteriaAllArray() {
        findKriteriaAll();
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getNama();
        }
        return datas;
    }
    
     public String[] findBerkasAllArray() {
        findBerkasAll();
        String[] datas = new String[berkass == null ? 0 : berkass.size()];
        for (int i = 0; berkass != null && i < berkass.size(); i++) {
            datas[i] = berkass.get(i).getNama();
        }
        return datas;
    }
     
   
     
    public Object[][] findByBerkasAll(Berkas berkas) 
    {
        findByBerkas(berkas);
        Object[][] datass = new Object[penilaianRelasiKriterias == null ? 0 : penilaianRelasiKriterias.size()][4];
       
        for (int row = 0; penilaianRelasiKriterias != null && row < penilaianRelasiKriterias.size(); row++) 
        {
            datass[row][0] = penilaianRelasiKriterias.get(row).getId();
            datass[row][1] = penilaianRelasiKriterias.get(row).getBerkas().getNama();
            datass[row][2] = penilaianRelasiKriterias.get(row).getKriteriaD().getNama();
            datass[row][3] = penilaianRelasiKriterias.get(row).getKriteriaM().getNama();
                    
        }
        return datass;
    }

    public Kriteria findKriteriaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(comboIndex);
    }
     
    public Berkas findBerkasFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || berkass == null) {
            return null;
        }
        return berkass.get(comboIndex);
    }
}
