/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.PenyediaDao;
import com.farindika.db.entity.Penyedia;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class PenyediaController {

    private PenyediaDao penyediaDao;
    private List<Penyedia> penyedias;

    public PenyediaController() {
        penyediaDao = HibernateUtil.getPenyediaDao();
    }

    public void save(Penyedia penyedia) {
        penyediaDao.save(penyedia);
    }

    public void update(Penyedia penyedia) {
        penyediaDao.update(penyedia);
    }

    public void delete(Penyedia penyedia) {
        penyediaDao.delete(penyedia);
    }
    
    public void deleteByTableIndex(int tableIndex){
        delete(findPenyediaFromTableIndex(tableIndex));
    }

    public List<Penyedia> findAll() 
    {
         penyedias = penyediaDao.findAll();
        return penyedias;
       // return penyediaDao.findAll();
    }

    public Object[][] findAllArray() {
        findAll();
        //List<Penyedia> penyedias = findAll();
        Object[][] datas = new Object[penyedias == null ? 0 : penyedias.size()][8];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; penyedias != null && row < penyedias.size(); row++) {
            datas[row][0] = penyedias.get(row).getId();
            datas[row][1] = penyedias.get(row).getNama();
            datas[row][2] = penyedias.get(row).getAlamat();
            datas[row][3] = penyedias.get(row).getTelp();
            datas[row][4] = penyedias.get(row).getEmail();
            datas[row][5] = dateFormat.format(penyedias.get(row).getTanggalDaftar().getTime());
            datas[row][6] = dateFormat.format(penyedias.get(row).getTanggalUpdate().getTime());
            datas[row][7] = penyedias.get(row).getKeterangan();
        }
        return datas;
    }
    
    public Penyedia findPenyediaFromTableIndex(int tableIndex)
    {
        if(tableIndex < 0 || penyedias == null)
        {
            return null;
        }
        return penyedias.get(tableIndex);
    }
}
