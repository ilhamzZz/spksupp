/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.BerkasDao;
import com.farindika.db.dao.KlasterDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Klaster;
import java.text.SimpleDateFormat;
import java.util.List;
/**
 *
 * @author Dhika
 */
public class KlasterController 
{
    private KlasterDao klasterDao;
    private BerkasDao berkasDao;
    private List<Klaster> klasters;
    private List<Berkas> berkass;
    
    public KlasterController() 
    {
        klasterDao = HibernateUtil.getKlasterDao();
        berkasDao = HibernateUtil.getBerkasDao();
    }

    public void save(Klaster klaster) 
    {
        klasterDao.save(klaster);
    }

    public void update(Klaster klaster) 
    {
        klasterDao.update(klaster);
    }

    public void delete(Klaster klaster) 
    {
        klasterDao.delete(klaster);
    }
    
    /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findKlasterFromTableIndex(tableIndex));
    }

    public List<Klaster> findAll() 
    {
        klasters = klasterDao.findAll();
        return klasters;
    }
    
    public List<Klaster> findByBerkas(Berkas b) 
    {
        klasters = klasterDao.findByBerkas(b);
        return klasters;
    }

    public Klaster findKlasterFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || klasters == null) 
        {
            return null;
        }
        return klasters.get(tableIndex);
    }

    public List<Berkas> findBerkasAll() 
    {
        berkass = berkasDao.findAll();
        return berkass;
    }

    public Object[][] findAllArray() 
    {
        findAll();
        Object[][] datas = new Object[klasters == null ? 0 : klasters.size()][5];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; klasters != null && row < klasters.size(); row++) 
        {
            Klaster klaster = klasters.get(row);
            datas[row][0] = klaster.getId();
            datas[row][1] = klaster.getKode();
            datas[row][2] = klaster.getNama();
            datas[row][3] = "";
            if(klaster.getBerkas()!= null)
            {
                datas[row][3]=klasters.get(row).getBerkas().getNama();
            }
            datas[row][4] = klaster.getKeterangan();
//           
        }
        return datas;
    }
    
       public Object[][] findAllBerkasArray(Berkas b) 
    {
        findByBerkas(b);
        Object[][] datas = new Object[klasters == null ? 0 : klasters.size()][5];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; klasters != null && row < klasters.size(); row++) 
        {
            datas[row][0] = klasters.get(row).getId();
            datas[row][1] = klasters.get(row).getKode();
            datas[row][2] = klasters.get(row).getNama();
            datas[row][3] = klasters.get(row).getBerkas().getNama();
            datas[row][4] = klasters.get(row).getKeterangan();

        }
        return datas;
    }

    public String[] findBerkasAllArray() 
    {
        findBerkasAll();
        String[] datas = new String[berkass == null ? 0 : berkass.size()];
        for (int i = 0; berkass != null && i < berkass.size(); i++) 
        {
            datas[i] = berkass.get(i).getNama();
        }
        return datas;
    }

    public Berkas findBerkasFromComboIndex(int comboIndex) 
    {
        if (comboIndex < 0 || berkass == null) 
        {
            return null;
        }
        return berkass.get(comboIndex);
    }

}
