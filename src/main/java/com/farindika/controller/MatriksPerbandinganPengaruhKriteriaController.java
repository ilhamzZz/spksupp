/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.BerkasDao;
import com.farindika.db.dao.KriteriaDao;
import com.farindika.db.dao.MatriksPerbandinganPengaruhKriteriaDao;
import com.farindika.db.dao.PenilaianRelasiKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganPengaruhKriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class MatriksPerbandinganPengaruhKriteriaController 
{
    private MatriksPerbandinganPengaruhKriteriaDao matriksPerbandinganPengaruhKriteriaDao;
    private PenilaianRelasiKriteriaDao penilaianRelasiKriteriaDao;
    private BerkasDao berkasDao;
    private List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias;
    private List<PenilaianRelasiKriteria> penilaianRelasiKriterias;
    private List<Berkas> berkass;
    private List<Kriteria>kriterias;
    
    public MatriksPerbandinganPengaruhKriteriaController() 
    {
        matriksPerbandinganPengaruhKriteriaDao = HibernateUtil.getMatriksPerbandinganPengaruhKriteriaDao();
        penilaianRelasiKriteriaDao = HibernateUtil.getPenilaianRelasiKriteriaDao();
        berkasDao = HibernateUtil.getBerkasDao();
    }
     
    public void save(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) 
    {
        matriksPerbandinganPengaruhKriteriaDao.save(matriksPerbandinganPengaruhKriteria);
    }
    
    public void save(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias) {
         matriksPerbandinganPengaruhKriteriaDao.save(matriksPerbandinganPengaruhKriterias);
    }

    public void update(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) 
    {
        matriksPerbandinganPengaruhKriteriaDao.update(matriksPerbandinganPengaruhKriteria);
    }
    
    public void update(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias) {
         matriksPerbandinganPengaruhKriteriaDao.update(matriksPerbandinganPengaruhKriterias);
    }

    public void delete(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) 
    {
        matriksPerbandinganPengaruhKriteriaDao.delete(matriksPerbandinganPengaruhKriteria);
    }
    
    public void delete(Berkas berkas, Kriteria kriteria)
    {
        matriksPerbandinganPengaruhKriteriaDao.delete(berkas, kriteria);
    }
    
     /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findMatriksPerbandinganPengaruhKriteriaFromTableIndex(tableIndex));
    }

    public List<MatriksPerbandinganPengaruhKriteria> findAll() 
    {
        matriksPerbandinganPengaruhKriterias = matriksPerbandinganPengaruhKriteriaDao.findAll();
        return matriksPerbandinganPengaruhKriterias;
    }
    
    public List<MatriksPerbandinganPengaruhKriteria> findByBerkasKTDipengaruhi(Berkas berkas, Kriteria kriteria)
    {
        matriksPerbandinganPengaruhKriterias = matriksPerbandinganPengaruhKriteriaDao.findByBerkasKTDipengaruhi(berkas, kriteria);
        return matriksPerbandinganPengaruhKriterias;
    }
    
    public List<Berkas> findBerkasAll()
    {
        berkass = berkasDao.findAll();
        return berkass;
    }
    
    public List<Kriteria> findKriteriaByBerkasKDipengaruhiAll(Berkas berkas, Kriteria kriteria)
    {
        kriterias = matriksPerbandinganPengaruhKriteriaDao.findKriteriaByBerkasKTDipengaruhi(berkas, kriteria);
        return kriterias;
    }
    
    public MatriksPerbandinganPengaruhKriteria findMatriksPerbandinganPengaruhKriteriaFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || matriksPerbandinganPengaruhKriterias == null) 
        {
            return null;
        }
        return matriksPerbandinganPengaruhKriterias.get(tableIndex);
    }

    public List<PenilaianRelasiKriteria> findPenilaianRelasiKriteriaAll() 
    {
        penilaianRelasiKriterias = penilaianRelasiKriteriaDao.findAll();
        return penilaianRelasiKriterias;
    }
    
    public Object[][] findAllArray() 
    {
        findAll();
        Object[][] datas = new Object[matriksPerbandinganPengaruhKriterias == null ? 0 : matriksPerbandinganPengaruhKriterias.size()][4];
        for (int row = 0; matriksPerbandinganPengaruhKriterias != null && row < matriksPerbandinganPengaruhKriterias.size(); row++) 
        {
            datas[row][0] = matriksPerbandinganPengaruhKriterias.get(row).getId();
            datas[row][1] = matriksPerbandinganPengaruhKriterias.get(row).getRelasi1_id().getId();
            datas[row][2] = matriksPerbandinganPengaruhKriterias.get(row).getRelasi2_id().getId();
            datas[row][3] = matriksPerbandinganPengaruhKriterias.get(row).getNilai();
        }
        return datas;
    }
    
    public Object[][] findByBerkasKTDipengaruhiAll(Berkas berkas, Kriteria kriteria)
    {
        findByBerkasKTDipengaruhi(berkas, kriteria);
        Object[][] datas = new Object[matriksPerbandinganPengaruhKriterias == null ? 0 : matriksPerbandinganPengaruhKriterias.size()][4];
        for (int row = 0; matriksPerbandinganPengaruhKriterias != null && row < matriksPerbandinganPengaruhKriterias.size(); row++) 
        {
            datas[row][0] = matriksPerbandinganPengaruhKriterias.get(row).getId();
            datas[row][1] = matriksPerbandinganPengaruhKriterias.get(row).getRelasi1_id().getId();
            datas[row][2] = matriksPerbandinganPengaruhKriterias.get(row).getRelasi2_id().getId();
            datas[row][3] = matriksPerbandinganPengaruhKriterias.get(row).getNilai();
        }
        return datas;
    }

    public String[] findPenilaianRelasiKriteriaAllArray() {
        findPenilaianRelasiKriteriaAll();
        String[] datas = new String[penilaianRelasiKriterias == null ? 0 : penilaianRelasiKriterias.size()];
        for (int i = 0; penilaianRelasiKriterias != null && i < penilaianRelasiKriterias.size(); i++) {
            datas[i] = penilaianRelasiKriterias.get(i).getKriteriaD().getNama();
        }
        return datas;
    }
    
    public PenilaianRelasiKriteria findPenilaianRelasiKriteriaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || penilaianRelasiKriterias == null) {
            return null;
        }
        return penilaianRelasiKriterias.get(comboIndex);
    }
    
    public Berkas findBerkasFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || berkass == null) {
            return null;
        }
        return berkass.get(comboIndex);
    }
    
    public Kriteria findKriteriaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(comboIndex);
    }
     
    public PenilaianRelasiKriteria findKTDipengaruhiFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || penilaianRelasiKriterias == null) {
            return null;
        }
        return penilaianRelasiKriterias.get(comboIndex);
    }
    
    public List<MatriksPerbandinganPengaruhKriteria> getMatriksPerbandinganPengaruhKriterias() {
        return matriksPerbandinganPengaruhKriterias;
    }
   
    
}
