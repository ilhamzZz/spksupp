/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.BerkasDao;
import com.farindika.db.dao.PenggunaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Pengguna;
import java.text.SimpleDateFormat;
import java.util.List;
/**
 *
 * @author Dhika
 */
public class BerkasController 
{
    private PenggunaDao penggunaDao;
    private BerkasDao berkasDao;
    private List<Pengguna> penggunas;
    private List<Berkas> berkass;

    public BerkasController() 
    {
        penggunaDao = HibernateUtil.getPenggunaDao();
        berkasDao = HibernateUtil.getBerkasDao();
    }

    public void save(Berkas berkas) 
    {
        berkasDao.save(berkas);
    }

    public void update(Berkas berkas) 
    {
        berkasDao.update(berkas);
    }

    public void delete(Berkas berkas) 
    {
        berkasDao.delete(berkas);
    }

     /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findBerkasFromTableIndex(tableIndex));
    }

    public List<Berkas> findAll() 
    {
        berkass = berkasDao.findAll();
        return berkass;
    }

    public Berkas findBerkasFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || berkass == null) 
        {
            return null;
        }
        return berkass.get(tableIndex);
    }

    public List<Pengguna> findPenggunaAll() 
    {
        penggunas = penggunaDao.findAll();
        return penggunas;
    }

    public Object[][] findAllArray() 
    {
        findAll();
        Object[][] datas = new Object[berkass == null ? 0 : berkass.size()][6];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; berkass != null && row < berkass.size(); row++) {
            datas[row][0] = berkass.get(row).getId();
            datas[row][1] = berkass.get(row).getNama();
            datas[row][2] = berkass.get(row).getTanggalBerkas()== null ? null : dateFormat.format(berkass.get(row).getTanggalBerkas().getTime());
            datas[row][3] = berkass.get(row).getTanggalUpdate()== null ? null : dateFormat.format(berkass.get(row).getTanggalUpdate().getTime());
            datas[row][4] = berkass.get(row).getPengguna().getName();
            datas[row][5] = berkass.get(row).getKeterangan();
//            datas[row][6] = berkass.get(row).getLastLogin() == null ? null : dateFormat.format(penggunas.get(row).getLastLogin().getTime());
        }
        return datas;
    }

    public String[] findPenggunaAllArray() {
        findPenggunaAll();
        String[] datas = new String[penggunas == null ? 0 : penggunas.size()];
        for (int i = 0; penggunas != null && i < penggunas.size(); i++) {
            datas[i] = penggunas.get(i).getName();
        }
        return datas;
    }

    public Pengguna findPenggunaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || penggunas == null) {
            return null;
        }
        return penggunas.get(comboIndex);
    }
}
