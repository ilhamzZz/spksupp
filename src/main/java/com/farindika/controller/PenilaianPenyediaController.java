/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.KriteriaDao;
import com.farindika.db.dao.PenilaianPenyediaDao;
import com.farindika.db.dao.PenyediaDao;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianPenyedia;
import com.farindika.db.entity.Penyedia;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class PenilaianPenyediaController 
{
    private PenyediaDao penyediaDao;
    private KriteriaDao kriteriaDao;
    private PenilaianPenyediaDao penilaianPenyediaDao;
    private List<Penyedia> penyedias;
    private List<Kriteria> kriterias;
    private List<PenilaianPenyedia> penilaianPenyedias;
    
    public PenilaianPenyediaController() 
    {
        penyediaDao = HibernateUtil.getPenyediaDao();
        kriteriaDao = HibernateUtil.getKriteriaDao();
        penilaianPenyediaDao = HibernateUtil.getPenilaianPenyediaDao();
    }
    
    public void save(PenilaianPenyedia penilaianPenyedia) 
    {
        penilaianPenyediaDao.save(penilaianPenyedia);
    }

    public void update(PenilaianPenyedia penilaianPenyedia) 
    {
        penilaianPenyediaDao.update(penilaianPenyedia);
    }
    
    
    public void save(List<PenilaianPenyedia> penilaianPenyedias) {
         penilaianPenyediaDao.save(penilaianPenyedias);
    }
    
    public void update(List<PenilaianPenyedia> penilaianPenyedias) {
         penilaianPenyediaDao.update(penilaianPenyedias);
    }
    

    public void delete(PenilaianPenyedia penilaianPenyedia) 
    {
        penilaianPenyediaDao.delete(penilaianPenyedia);
    }
    
    public void delete(Penyedia penyedia) 
    {
        penilaianPenyediaDao.delete(penyedia);
    }

    
     /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findPenilaianPenyediaFromTableIndex(tableIndex));
    }

    public List<PenilaianPenyedia> findAll() 
    {
        penilaianPenyedias = penilaianPenyediaDao.findAll();
        return penilaianPenyedias;
    }
    
     public List<PenilaianPenyedia> findByPenyedia(Penyedia penyedia) 
    {
        penilaianPenyedias = penilaianPenyediaDao.findByPenyedia(penyedia);
        return penilaianPenyedias;
    }

    public PenilaianPenyedia findPenilaianPenyediaFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || penilaianPenyedias == null) 
        {
            return null;
        }
        return penilaianPenyedias.get(tableIndex);
    }

    public List<Penyedia> findPenyediaAll() 
    {
        penyedias = penyediaDao.findAll();
        return penyedias;
    }
    
    public List<Kriteria> findKriteriaAll() 
    {
        kriterias = kriteriaDao.findAll();
        return kriterias;
    }

    public Object[][] findAllArray() 
    {
        findAll();
        Object[][] datas = new Object[penilaianPenyedias == null ? 0 : penilaianPenyedias.size()][4];
       
        for (int row = 0; penilaianPenyedias != null && row < penilaianPenyedias.size(); row++) {
            datas[row][0] = penilaianPenyedias.get(row).getId();
            datas[row][1] = penilaianPenyedias.get(row).getPenyedia().getNama();
            datas[row][2] = penilaianPenyedias.get(row).getKriteria().getNama();
            datas[row][3] = penilaianPenyedias.get(row).getNilai();
            
        }
        return datas;
    }
    
     public Object[][] findAllKriteriaArray() 
    {
        findKriteriaAll();
        Object[][] datass = new Object[kriterias == null ? 0 : kriterias.size()][3];
       
        for (int row = 0; kriterias != null && row < kriterias.size(); row++) 
        {
            datass[row][0] = kriterias.get(row).getNama();
            datass[row][1] = kriterias.get(row).getKode();
            datass[row][2] = new Integer(0);
                    
        }
        return datass;
    }
     
    public Object[][] findByPenyediaAll(Penyedia penyedia) 
    {
        findByPenyedia(penyedia);
        Object[][] datass = new Object[penilaianPenyedias == null ? 0 : penilaianPenyedias.size()][3];
       
        for (int row = 0; penilaianPenyedias != null && row < penilaianPenyedias.size(); row++) 
        {
            datass[row][0] = penilaianPenyedias.get(row).getKriteria().getNama();
            datass[row][1] = penilaianPenyedias.get(row).getKriteria().getKode();
            datass[row][2] = penilaianPenyedias.get(row).getNilai();
                    
        }
        return datass;
    }
     

    public String[] findPenyediaAllArray() {
        findPenyediaAll();
        String[] datas = new String[penyedias == null ? 0 : penyedias.size()];
        for (int i = 0; penyedias != null && i < penyedias.size(); i++) {
            datas[i] = penyedias.get(i).getNama();
        }
        return datas;
    }

    public Penyedia findPenyediaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || penyedias == null) {
            return null;
        }
        return penyedias.get(comboIndex);
    }
    
     public String[] findKriteriaAllArray() {
        findKriteriaAll();
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getNama();
        }
        return datas;
    }
     
    public String[] findKodeKriteriaAllArray() {
        findKriteriaAll();
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getKode();
        }
        return datas;
    }

    public Kriteria findKriteriaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(comboIndex);
    }
    
    public Kriteria findKriteriaFromTableIndex(int tableIndex) {
        if (tableIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(tableIndex);
    }

    public List<Kriteria> getKriterias() {
        return kriterias;
    }

    public List<PenilaianPenyedia> getPenilaianPenyedias() {
        return penilaianPenyedias;
    }

    public List<Penyedia> getPenyedias() {
        return penyedias;
    }

    
}
