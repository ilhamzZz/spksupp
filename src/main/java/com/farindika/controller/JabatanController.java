/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.JabatanDao;
import com.farindika.db.entity.Jabatan;
import java.util.List;

/**
 *
 * @author Latief
 */
public class JabatanController {

    private JabatanDao jabatanDao;
    private List<Jabatan> jabatans;

    public JabatanController() {
        jabatanDao = HibernateUtil.getJabatanDao();
    }

    public void save(Jabatan jabatan) {
        jabatanDao.save(jabatan);
    }

    public void update(Jabatan jabatan) {
        jabatanDao.update(jabatan);
    }

    public void delete(Jabatan jabatan) {
        jabatanDao.delete(jabatan);
    }

    /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    public void deleteByTableIndex(int tableIndex) {
        delete(findJabatanFromTableIndex(tableIndex));
    }

    public List<Jabatan> findAll() {
        jabatans = jabatanDao.findAll();
        return jabatans;
    }

    /**
     * Cari semua data Jabatan dengan format yang sudah dikonversi ke array 2
     * dimensi. Digunakan untuk ditampulkan pada table.
     *
     * @return
     */
    public Object[][] findAllArray() {
        findAll();
        Object[][] datas = new Object[jabatans == null ? 0 : jabatans.size()][3];
        for (int row = 0; jabatans != null && row < jabatans.size(); row++) {
            datas[row][0] = jabatans.get(row).getId();
            datas[row][1] = jabatans.get(row).getNama();
            datas[row][2] = jabatans.get(row).getKeterangan();
        }
        return datas;
    }

    /**
     * Dapatkan
     * <code>Jabatan</code> berdasarkan urutan pada table jabatan.
     *
     * @param tableIndex
     * @return
     */
    public Jabatan findJabatanFromTableIndex(int tableIndex) {
        if (tableIndex < 0 || jabatans == null) {
            return null;
        }
        return jabatans.get(tableIndex);
    }
}
