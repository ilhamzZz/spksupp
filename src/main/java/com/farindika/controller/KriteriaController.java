/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.KriteriaDao;
import com.farindika.db.entity.Kriteria;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class KriteriaController 
{
     private KriteriaDao kriteriaDao;
    private List<Kriteria> kriterias;

    public KriteriaController() {
        kriteriaDao = HibernateUtil.getKriteriaDao();
    }

    public void save(Kriteria kriteria) {
        kriteriaDao.save(kriteria);
    }

    public void update(Kriteria kriteria) {
        kriteriaDao.update(kriteria);
    }

    public void delete(Kriteria kriteria) {
        kriteriaDao.delete(kriteria);
    }

    /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    public void deleteByTableIndex(int tableIndex) {
        delete(findKriteriaFromTableIndex(tableIndex));
    }

    public List<Kriteria> findAll() {
        kriterias = kriteriaDao.findAll();
        return kriterias;
    }

    /**
     * Cari semua data Kriteria dengan format yang sudah dikonversi ke array 2
     * dimensi. Digunakan untuk ditampulkan pada table.
     *
     * @return
     */
    public Object[][] findAllArray() {
        findAll();
        Object[][] datas = new Object[kriterias == null ? 0 : kriterias.size()][4];
        for (int row = 0; kriterias != null && row < kriterias.size(); row++) {
            datas[row][0] = kriterias.get(row).getId();
            datas[row][1] = kriterias.get(row).getNama();
            datas[row][2] = kriterias.get(row).getKode();
            datas[row][3] = kriterias.get(row).getKeterangan();
        }
        return datas;
    }

    /**
     * Dapatkan
     * <code>Kriteria</code> berdasarkan urutan pada table kriteria.
     *
     * @param tableIndex
     * @return
     */
    public Kriteria findKriteriaFromTableIndex(int tableIndex) {
        if (tableIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(tableIndex);
    }
}
