/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.BerkasDao;
import com.farindika.db.dao.KriteriaDao;
import com.farindika.db.dao.MatriksPerbandinganKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganKriteria;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class MatriksPerbandinganKriteriaController 
{
    private KriteriaDao kriteriaDao;
    private BerkasDao berkasDao;
    private MatriksPerbandinganKriteriaDao matriksPerbandinganKriteriaDao;
    private List<Kriteria> kriterias;
    private List<Kriteria> kriteriasB;
    private List<Berkas> berkass;
    private List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias;
    
    public MatriksPerbandinganKriteriaController () 
    {
        kriteriaDao = HibernateUtil.getKriteriaDao();
        berkasDao = HibernateUtil.getBerkasDao();
        matriksPerbandinganKriteriaDao = HibernateUtil.getMatriksPerbandinganKriteriaDao();
    }
     
    public void save(MatriksPerbandinganKriteria matriksPerbandinganKriteria) 
    {
        matriksPerbandinganKriteriaDao.save(matriksPerbandinganKriteria);
    }
    
    public void save(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) {
         matriksPerbandinganKriteriaDao.save(matriksPerbandinganKriterias);
    }

    public void update(MatriksPerbandinganKriteria matriksPerbandinganKriteria) 
    {
        matriksPerbandinganKriteriaDao.update(matriksPerbandinganKriteria);
    }
    
    public void update(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) {
         matriksPerbandinganKriteriaDao.update(matriksPerbandinganKriterias);
    }

    public void delete(MatriksPerbandinganKriteria matriksPerbandinganKriteria) 
    {
        matriksPerbandinganKriteriaDao.delete(matriksPerbandinganKriteria);
    }
    
    public void delete(Berkas berkas) 
    {
        matriksPerbandinganKriteriaDao.delete(berkas);
    }
    
     /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    
    public void deleteByTableIndex(int tableIndex) 
    {
        delete(findMatriksPerbandinganKriteriaFromTableIndex(tableIndex));
    }

    public List<MatriksPerbandinganKriteria> findAll() 
    {
        matriksPerbandinganKriterias = matriksPerbandinganKriteriaDao.findAll();
        return matriksPerbandinganKriterias;
    }
    
    public List<Kriteria> findKriteriaByBerkas(Berkas berkas) 
    {
        kriteriasB = matriksPerbandinganKriteriaDao.findKriteriaByBerkas(berkas);
        return kriteriasB;
    }

    public MatriksPerbandinganKriteria findMatriksPerbandinganKriteriaFromTableIndex(int tableIndex) 
    {
        if (tableIndex < 0 || matriksPerbandinganKriterias == null) 
        {
            return null;
        }
        return matriksPerbandinganKriterias.get(tableIndex);
    }

    public List<Kriteria> findKriteriaAll() 
    {
        kriterias = kriteriaDao.findAll();
        return kriterias;
    }
    
      public List<Berkas> findBerkasAll() 
    {
        berkass = berkasDao.findAll();
        return berkass;
    }

    public List<MatriksPerbandinganKriteria> findByBerkas(Berkas berkas) 
    {
        matriksPerbandinganKriterias = matriksPerbandinganKriteriaDao.findByBerkas(berkas);
        return matriksPerbandinganKriterias;
    }
    public Object[][] findAllArray() 
    {
        findAll();
        Object[][] datas = new Object[matriksPerbandinganKriterias == null ? 0 : matriksPerbandinganKriterias.size()][5];
        for (int row = 0; matriksPerbandinganKriterias != null && row < matriksPerbandinganKriterias.size(); row++) 
        {
            datas[row][0] = matriksPerbandinganKriterias.get(row).getId();
            datas[row][1] = matriksPerbandinganKriterias.get(row).getKriteria1().getNama();
            datas[row][2] = matriksPerbandinganKriterias.get(row).getKriteria2().getNama();
            datas[row][3] = matriksPerbandinganKriterias.get(row).getNilai();
            datas[row][4] = matriksPerbandinganKriterias.get(row).getBerkas().getNama();
        }
        return datas;
    }
    
    public Object[][] findByBerkasAll(Berkas berkas) 
    {
        findByBerkas(berkas);
        Object[][] datass = new Object[matriksPerbandinganKriterias == null ? 0 : matriksPerbandinganKriterias.size()][5];
       
        for (int row = 0; matriksPerbandinganKriterias != null && row < matriksPerbandinganKriterias.size(); row++) 
        {
            datass[row][0] = matriksPerbandinganKriterias.get(row).getId();
            datass[row][1] = matriksPerbandinganKriterias.get(row).getBerkas().getNama();
            datass[row][2] = matriksPerbandinganKriterias.get(row).getKriteria1().getNama();
            datass[row][3] = matriksPerbandinganKriterias.get(row).getKriteria2().getNama();
            datass[row][4] = matriksPerbandinganKriterias.get(row).getNilai();
                    
        }
        return datass;
    }
    
    public Object[][] findByBerkasNilai(Berkas berkas) 
    {
        findByBerkas(berkas);
        Object[][] datass = new Object[matriksPerbandinganKriterias == null ? 0 : matriksPerbandinganKriterias.size()][3];
       
        for (int row = 0; matriksPerbandinganKriterias != null && row < matriksPerbandinganKriterias.size(); row++) 
        {
            
          
            datass[row][0] = matriksPerbandinganKriterias.get(row).getKriteria1().getNama();
            datass[row][1] = matriksPerbandinganKriterias.get(row).getKriteria2().getNama();
            datass[row][2] = matriksPerbandinganKriterias.get(row).getNilai();
                    
        }
        return datass;
    }

    public String[] findKriteriaAllArray() {
        findKriteriaAll();
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getKode();
        }
        return datas;
    }
    
    public String[] findNamaKriteriaAllArray() {
        findKriteriaAll();
        String[] datas = new String[kriterias == null ? 0 : kriterias.size()];
        for (int i = 0; kriterias != null && i < kriterias.size(); i++) {
            datas[i] = kriterias.get(i).getNama();
        }
        return datas;
    }
    
    public String[] findKodeKriteriaByBerkasAllArray(Berkas berkas) {
        findKriteriaByBerkas(berkas);
        String[] datas = new String[kriteriasB == null ? 0 : kriteriasB.size()];
        for (int i = 0; kriteriasB != null && i < kriteriasB.size(); i++) {
            datas[i] = kriteriasB.get(i).getKode();
        }
        return datas;
    }
    
    public String[] findNamaKriteriaByBerkasAllArray(Berkas berkas) {
        findKriteriaByBerkas(berkas);
        String[] datas = new String[kriteriasB == null ? 0 : kriteriasB.size()];
        for (int i = 0; kriteriasB != null && i < kriteriasB.size(); i++) {
            datas[i] = kriteriasB.get(i).getNama();
        }
        return datas;
    }
    
     public String[] findBerkasAllArray() {
        findBerkasAll();
        String[] datas = new String[berkass == null ? 0 : berkass.size()];
        for (int i = 0; berkass != null && i < berkass.size(); i++) {
            datas[i] = berkass.get(i).getNama();
        }
        return datas;
    }

    public Kriteria findKriteriaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || kriterias == null) {
            return null;
        }
        return kriterias.get(comboIndex);
    }
    
    public Kriteria findKriteriaBerkasFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || kriteriasB == null) {
            return null;
        }
        return kriteriasB.get(comboIndex);
    }
     
    public Berkas findBerkasFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || berkass == null) {
            return null;
        }
        return berkass.get(comboIndex);
    }

    public List<Berkas> getBerkass() {
        return berkass;
    }

    public void setBerkass(List<Berkas> berkass) {
        this.berkass = berkass;
    }

    public List<Kriteria> getKriterias() {
        return kriterias;
    }

    public void setKriterias(List<Kriteria> kriterias) {
        this.kriterias = kriterias;
    }

    public List<Kriteria> getKriteriasB() {
        return kriteriasB;
    }

    public void setKriteriasB(List<Kriteria> kriteriasB) {
        this.kriteriasB = kriteriasB;
    }
    
    

    public MatriksPerbandinganKriteriaDao getMatriksPerbandinganKriteriaDao() {
        return matriksPerbandinganKriteriaDao;
    }

    public void setMatriksPerbandinganKriteriaDao(MatriksPerbandinganKriteriaDao matriksPerbandinganKriteriaDao) {
        this.matriksPerbandinganKriteriaDao = matriksPerbandinganKriteriaDao;
    }

    public List<MatriksPerbandinganKriteria> getMatriksPerbandinganKriterias() {
        return matriksPerbandinganKriterias;
    }

    public void setMatriksPerbandinganKriterias(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) {
        this.matriksPerbandinganKriterias = matriksPerbandinganKriterias;
    }
    
    
}
