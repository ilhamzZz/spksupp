/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.JabatanDao;
import com.farindika.db.dao.PenggunaDao;
import com.farindika.db.entity.Jabatan;
import com.farindika.db.entity.Pengguna;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class PenggunaController {

    private PenggunaDao penggunaDao;
    private JabatanDao jabatanDao;
    private List<Pengguna> penggunas;
    private List<Jabatan> jabatans;
    private Pengguna pengguna;

    public PenggunaController() {
        penggunaDao = HibernateUtil.getPenggunaDao();
        jabatanDao = HibernateUtil.getJabatanDao();
    }

    public void save(Pengguna pengguna) {
        penggunaDao.save(pengguna);
    }

    public void update(Pengguna pengguna) {
        penggunaDao.update(pengguna);
    }

    public void delete(Pengguna pengguna) {
        penggunaDao.delete(pengguna);
    }

    /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    public void deleteByTableIndex(int tableIndex) {
        delete(findPenggunaFromTableIndex(tableIndex));
    }

    public List<Pengguna> findAll() {
        penggunas = penggunaDao.findAll();
        return penggunas;
    }

    public Pengguna findPenggunaFromTableIndex(int tableIndex) {
        if (tableIndex < 0 || penggunas == null) {
            return null;
        }
        return penggunas.get(tableIndex);
    }
    
    public Pengguna findPenggunaByUsernamePassword(String UserName, String Password)
    {
        pengguna = penggunaDao.findPenggunaLogin(UserName, Password);
        return pengguna;
    }

    public List<Jabatan> findJabatanAll() {
        jabatans = jabatanDao.findAll();
        return jabatans;
    }

    public Object[][] findAllArray() {
        findAll();
        Object[][] datas = new Object[penggunas == null ? 0 : penggunas.size()][7];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; penggunas != null && row < penggunas.size(); row++) {
            datas[row][0] = penggunas.get(row).getId();
            datas[row][1] = penggunas.get(row).getJabatan().getNama();
            datas[row][2] = penggunas.get(row).getName();
            datas[row][3] = penggunas.get(row).getUserName();
            datas[row][4] = penggunas.get(row).getEmail();
            datas[row][5] = penggunas.get(row).getStatus();
            datas[row][6] = penggunas.get(row).getLastLogin() == null ? null : dateFormat.format(penggunas.get(row).getLastLogin().getTime());
        }
        return datas;
    }

    public String[] findJabatanAllArray() {
        findJabatanAll();
        String[] datas = new String[jabatans == null ? 0 : jabatans.size()];
        for (int i = 0; jabatans != null && i < jabatans.size(); i++) {
            datas[i] = jabatans.get(i).getNama();
        }
        return datas;
    }

    public Jabatan findJabatanFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || jabatans == null) {
            return null;
        }
        return jabatans.get(comboIndex);
    }
}
