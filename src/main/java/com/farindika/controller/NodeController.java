/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.controller;

import com.farindika.db.HibernateUtil;
import com.farindika.db.dao.KlasterDao;
import com.farindika.db.dao.NodeDao;
import com.farindika.db.dao.PenyediaDao;
import com.farindika.db.entity.Klaster;
import com.farindika.db.entity.Node;
import com.farindika.db.entity.Penyedia;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class NodeController {

    private PenyediaDao penyediaDao;
    private NodeDao nodeDao;
    private KlasterDao klasterDao;
    private List<Klaster> klasters;
    private List<Penyedia> penyedias;
    private List<Node> nodes;

    public NodeController() {
        klasterDao = HibernateUtil.getKlasterDao();
        penyediaDao = HibernateUtil.getPenyediaDao();
        nodeDao = HibernateUtil.getNodeDao();
    }

    public void save(Node node) {
        nodeDao.save(node);
    }

    public void update(Node node) {
        nodeDao.update(node);
    }

    public void delete(Node node) {
        nodeDao.delete(node);
    }

    /**
     * Hapus berdasarkan index pada table.
     *
     * @param tableIndex
     */
    public void deleteByTableIndex(int tableIndex) {
        delete(findNodeFromTableIndex(tableIndex));
    }

    public List<Node> findAll() {
        nodes = nodeDao.findAll();
        return nodes;
    }

    public Node findNodeFromTableIndex(int tableIndex) {
        if (tableIndex < 0 || nodes == null) {
            return null;
        }
        return nodes.get(tableIndex);
    }

    public List<Klaster> findKlasterAll() {
        klasters = klasterDao.findAll();
        return klasters;
    }

    public List<Penyedia> findPenyediaAll() {
        penyedias = penyediaDao.findAll();
        return penyedias;
    }

    public Object[][] findAllArray() {
        findAll();
        Object[][] datas = new Object[nodes == null ? 0 : nodes.size()][6];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (int row = 0; nodes != null && row < nodes.size(); row++) {
            Node node = nodes.get(row);
            datas[row][0] = node.getId();
            datas[row][1] = node.getKode();
            datas[row][2] = node.getNama();
            datas[row][3] = node.getKlaster().getNama();
            datas[row][4] = node.getKeterangan();
            datas[row][5] = "";
            if (node.getPenyedia() != null) {
                datas[row][5] = nodes.get(row).getPenyedia().getNama(); // Errornya disini... knp ya?
            }
        }
        return datas;
    }

    public String[] findKlasterAllArray() {
        findKlasterAll();
        String[] datas = new String[klasters == null ? 0 : klasters.size()];
        for (int i = 0; klasters != null && i < klasters.size(); i++) {
            datas[i] = klasters.get(i).getNama();
        }
        return datas;
    }

    public Klaster findKlasterFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || klasters == null) {
            return null;
        }
        return klasters.get(comboIndex);
    }

    public String[] findPenyediaAllArray() {
        findPenyediaAll();
        String[] datas = new String[penyedias == null ? 0 : penyedias.size()];
        for (int i = 0; penyedias != null && i < penyedias.size(); i++) {
            datas[i] = penyedias.get(i).getNama();
        }
        return datas;
    }

    public Penyedia findPenyediaFromComboIndex(int comboIndex) {
        if (comboIndex < 0 || penyedias == null) {
            return null;
        }
        return penyedias.get(comboIndex);
    }
}
