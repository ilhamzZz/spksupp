/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.calculate.util;

/**
 *
 * @author Ultra
 */
public final class CalculateUtil {

    public static double[] normalisasi(double[] matriks) {
        double[] hasil = new double[matriks.length];
        double sum = 0d;
        for (double numeric : matriks) {
            sum += numeric;
        }
        for (int i = 0; i < matriks.length; i++) {
            hasil[i] = matriks[i] / sum;
        }
        return hasil;
    }

    public static double[][] normalisasi(double[][] matrix) {
        /*
         * sengaja ga ditampilkan, ini untuk lihat matriks pairwise node
         */
        double[][] hasil = new double[matrix.length][matrix.length];
        for (int col = 0; col < matrix.length; col++) {
            double sum = 0d;
            for (int row = 0; row < matrix.length; row++) {
                sum += matrix[row][col];
            }
            for (int row = 0; row < matrix.length; row++) {
                hasil[row][col] = (sum == 0 ? 0 : round(matrix[row][col] / sum, 2));
            }
        }
        return hasil;
    }

    public static double average(double[] values) {
        double sum = 0d;
        for (double num : values) {
            sum += num;
        }

        return sum / values.length;
    }

    public static double[] average(double[][] values) {
        double[] hasil = new double[values.length];
        for (int row = 0; row < values.length; row++) {
            int colCount = 0;
            double sum = 0d;
            for (int col = 0; col < values.length; col++) {
                if (values[row][col] == 0) {
                    colCount++;
                }
                sum += values[row][col];
            }
            hasil[row] = sum / (values.length - colCount);
        }
        return hasil;
    }

    /**
     * perkalian untuk matrix bujur sangkar atau baris = kolom
     *
     * @param matrix1
     * @param matrix2
     * @return
     */
    public static double[][] multiplication(double[][] matrix1, double[][] matrix2) {
        double[][] result = new double[matrix1.length][matrix1.length];
        for (int h = 0; h < result.length; h++) {
            for (int i = 0; i < result.length; i++) {
                result[h][i] = 0;
                for (int j = 0; j < result.length; j++) {
                    result[h][i] += (matrix1[h][j] * matrix2[j][i]);
                }
                result[h][i] = result[h][i];
            }
        }
        return result;
    }
    
  /**
     * perkalian untuk matrix bujur sangkar atau baris != kolom
     *
     * @param matrix1
     * @param matrix2
     * @return
     */
    public static double[] multiplication(double[][] matrix1, double[] matrix2) {
        double[] result = new double[matrix2.length];
        for (int h = 0; h < result.length; h++) 
        {
            result[h] = 0;
            for (int i = 0; i < result.length; i++) 
            {
                
                result[h] +=  (matrix1[h][i] * matrix2[i]);
                             
            }
        }
        return result;
    }
    /**
     * perkalian untuk matrix bujur sangkar atau baris = kolom
     *
     * @param matrix
     * @return
     */
    public static double[][] pow(double[][] matrix) {
        return multiplication(matrix, matrix);
    }

    public static void showArray(double[][] arr) {
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr.length; col++) {
                System.out.print(arr[row][col] + "\t");
            }
            System.out.println("");
        }
    }

    public static void showArray(double[] arr) {
        for (int row = 0; row < arr.length; row++) {
            System.out.println(round(arr[row], 4));
        }
    }

    public static double round(double valueToRound, int numberOfDecimalPlaces) {
        double multipicationFactor = Math.pow(10, numberOfDecimalPlaces);
        double interestedInZeroDPs = valueToRound * multipicationFactor;
        return Math.round(interestedInZeroDPs) / multipicationFactor;
    }
    
    public static String [][] bubble_srt( double a[], int n, String b[], int m )
    {
        String [][] sort = new String[n][2];
        int i, j;
        double t=0;
        String ts = "";
        for(i = 0; i < n; i++)
        {
            for(j = 1; j < (n-i); j++)
            {
                if(a[j-1] < a[j])
                {
                    ts = b[j-1];
                    t = a[j-1];
                    
                    a[j-1]=a[j];
                    b[j-1]=b[j];
                    
                    a[j]=t;
                    b[j]=ts;
                }
            }
        }
        
        for(i = 0; i<n ;i++)
        {
            for(j = 0; j<2 ; j++)
            {
                if(j==0)
                {
                    sort[i][j]=b[i];
                }
                else if(j == 1)
                {
                    sort[i][j]=round(a[i],4)+"";
                }
            }
        }
        return sort;
    }
}
