/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.calculate;

import com.farindika.calculate.util.CalculateUtil;
import com.farindika.controller.MatriksPerbandinganPengaruhKriteriaController;
import com.farindika.controller.PenilaianRelasiKriteriaController;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganPengaruhKriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;

/**
 *
 * @author Ultra
 */
public class BOBOTANP implements Calculate{
    /*
     * Data masukan skor kriteria dan skor interdependence antar kriteria
     */
    
    private double [] skorKriteria;
    private double [][] skorInterdependenceKriteria;
    private double [][] bobotRelatifinterdependence;
    private double [][] nilaiKepentinganKriteria;
    public double RI;
    public double lambdaMax,CI,CR;
    
    /*
     * Data Hasil pembobotan kriteria dengan ANP
     */
    
    public double [] hasilPairwiseComparisonKriteria;
    private double [][] hasilPairwiseComparisonInterdependence;
    public double [] bobotRelatifKriteria;
    
    public void calculate() 
    {
        calculatePairwiseComparisonKriteria();
        calculateBobotRelatifKriteria();
        calculateConsistencyRatio( );
    }
    
    /*
     * Methods untuk kalkulasi pairwise kriteria dan interdependence kriteria
     */
    
    public void calculatePairwiseComparisonKriteria()
    {
        /*
         * Inisialisasi variable
         */
        hasilPairwiseComparisonKriteria = new double[nilaiKepentinganKriteria.length];
        double [][] pairwiseComparison = new double[nilaiKepentinganKriteria.length][nilaiKepentinganKriteria.length];
        double [] sum= new double[nilaiKepentinganKriteria.length];
        /*
         * Men-assign nilai 0 ke variable sum
         */
        for(int i = 0; i<nilaiKepentinganKriteria.length; i++)
        {
            sum[i] = 0d;
        }
        /*
         * membentuk pairwise comparison
         */
        for(int row = 0; row<nilaiKepentinganKriteria.length;row++)
        {
            
            for(int col = 0; col < nilaiKepentinganKriteria.length; col++)
            {
                pairwiseComparison[row][col]= nilaiKepentinganKriteria[row][col];
                sum[col]=CalculateUtil.round(sum[col]+pairwiseComparison[row][col],4);
            }
        }

        /*
         * menghitung bobot prioritas
         */
        double [][]initPriority = new double [nilaiKepentinganKriteria.length][nilaiKepentinganKriteria.length];
        double [] rowSum = new double [nilaiKepentinganKriteria.length];
         for(int i = 0; i<nilaiKepentinganKriteria.length; i++)
        {
            rowSum[i] = 0d;
        }
        for(int row = 0; row<nilaiKepentinganKriteria.length;row++)
        {
            
            for(int col = 0; col < nilaiKepentinganKriteria.length; col++)
            {
                initPriority[row][col]= CalculateUtil.round(pairwiseComparison[row][col]/sum[col], 4);
                rowSum[row]=CalculateUtil.round(rowSum[row]+initPriority[row][col],4);
            }
            hasilPairwiseComparisonKriteria[row] = rowSum[row]/nilaiKepentinganKriteria.length;
        }
        System.out.println("----------------------------bobot kriteria tanpa interdependensi-----------------------");
        CalculateUtil.showArray(hasilPairwiseComparisonKriteria);
        System.out.println("Nilai RI " + randomIndex());
        System.out.println("Nilai CR " + calculateConsistencyRatio( ));
        System.out.println("----------------------------bobot kriteria tanpa interdependensi-----------------------");
    }
    
    public void calculatePairwiseComparisonInterdependence()
    {
        
    }
    
    public void calculateBobotRelatifKriteria()
    {
        System.out.println("----------------------------bobot interdependensi antar kriteria-----------------------");
        CalculateUtil.showArray(bobotRelatifinterdependence);
        System.out.println("----------------------------bobot interdependensi antar kriteria-----------------------");
        bobotRelatifKriteria = new double[bobotRelatifinterdependence.length];
        bobotRelatifKriteria = CalculateUtil.multiplication(bobotRelatifinterdependence, hasilPairwiseComparisonKriteria);
        System.out.println("+++++++++++++++++++++++++++++ BOBOT RELATIF KRITERIA+++++++++++++++++++++++++++");
        CalculateUtil.showArray(bobotRelatifKriteria);
        System.out.println("+++++++++++++++++++++++++++++ BOBOT RELATIF KRITERIA+++++++++++++++++++++++++++");
    }
    
    public double randomIndex()
    {
        
        int jmlKriteria = nilaiKepentinganKriteria.length;
        RI = 0d;
        if (jmlKriteria == 0 ||jmlKriteria == 1 ||jmlKriteria == 2)
        {
            RI = 0;
        }
        else if(jmlKriteria == 3)
        {
            RI = 0.58;
        }
         else if(jmlKriteria == 4)
        {
            RI = 0.90;
        }
         else if(jmlKriteria == 5)
        {
            RI = 1.12;
        }
         else if(jmlKriteria == 6)
        {
            RI = 1.24;
        }
         else if(jmlKriteria == 7)
        {
            RI = 1.32;
        }
         else if(jmlKriteria == 8)
        {
            RI = 1.41;
        }
         else if(jmlKriteria == 9)
        {
            RI = 1.45;
        }
         else if(jmlKriteria == 10)
        {
            RI = 1.49;
        }
         else if(jmlKriteria == 11)
        {
            RI = 1.52;
        }
         else if(jmlKriteria == 12)
        {
            RI = 1.54;
        }
         else if(jmlKriteria == 13)
        {
            RI = 1.56;
        }
         else if(jmlKriteria == 14)
        {
            RI = 1.58;
        }
         else if(jmlKriteria == 15)
        {
            RI = 1.59;
        }
        
        return RI;
    }
    
    public double calculateConsistencyRatio( )
    {
        double[] sumRow = new double[nilaiKepentinganKriteria.length];
        double[] hasil = new double[nilaiKepentinganKriteria.length];
        double jumlahHasil=0d;
        
        double [][] elemenBaris = new double[nilaiKepentinganKriteria.length][nilaiKepentinganKriteria.length];
        
        /*
         * menjumlahkan elemen baris
         */
        for(int row = 0; row < nilaiKepentinganKriteria.length; row++)
        {
            for(int col = 0; col < nilaiKepentinganKriteria.length; col++)
            {
                elemenBaris[row][col] = CalculateUtil.round(nilaiKepentinganKriteria[row][col]*hasilPairwiseComparisonKriteria[col],4);
                sumRow[row] = sumRow[row]+elemenBaris[row][col];
            }
        }
       
        for(int i = 0; i<nilaiKepentinganKriteria.length; i++)
        {
            hasil[i]=CalculateUtil.round(sumRow[i]/hasilPairwiseComparisonKriteria[i],4);
            jumlahHasil += hasil[i];
        }
        /*
         * Menghitung LambdaMax dan CI
         */
        
        lambdaMax = jumlahHasil/nilaiKepentinganKriteria.length;
        CI = (lambdaMax - nilaiKepentinganKriteria.length)/(nilaiKepentinganKriteria.length-1);
         
        /*
         * Menghitung CR
         */
        CR = CalculateUtil.round(CI/randomIndex(),4);
        return CR;
    }
    /*
     * Setter Getter Properties
     */

    public double[] getBobotRelatifKriteria() {
        return bobotRelatifKriteria;
    }

    public double[][] getHasilPairwiseComparisonInterdependence() {
        return hasilPairwiseComparisonInterdependence;
    }

    public double[] getHasilPairwiseComparisonKriteria() {
        return hasilPairwiseComparisonKriteria;
    }

    public double[][] getSkorInterdependenceKriteria() {
        return skorInterdependenceKriteria;
    }

    public void setSkorInterdependenceKriteria(double[][] skorInterdependenceKriteria) {
        this.skorInterdependenceKriteria = skorInterdependenceKriteria;
    }

    public double[] getSkorKriteria() {
        return skorKriteria;
    }

    public void setSkorKriteria(double[] skorKriteria) {
        this.skorKriteria = skorKriteria;
    }

    public double[][] getBobotRelatifinterdependence() {
        return bobotRelatifinterdependence;
    }

    public void setBobotRelatifinterdependence(double[][] bobotRelatifinterdependence) {
        this.bobotRelatifinterdependence = bobotRelatifinterdependence;
    }

    public double[][] getNilaiKepentinganKriteria() {
        return nilaiKepentinganKriteria;
    }

    public void setNilaiKepentinganKriteria(double[][] nilaiKepentinganKriteria) {
        this.nilaiKepentinganKriteria = nilaiKepentinganKriteria;
    }

    public double getCI() {
        return CI;
    }

    public void setCI(double CI) {
        this.CI = CI;
    }

    public double getCR() {
        return CR;
    }

    public void setCR(double CR) {
        this.CR = CR;
    }

    public double getRI() {
        return RI;
    }

    public void setRI(double RI) {
        this.RI = RI;
    }

    public double getLambdaMax() {
        return lambdaMax;
    }

    public void setLambdaMax(double lambdaMax) {
        this.lambdaMax = lambdaMax;
    }

    public void setHasilPairwiseComparisonKriteria(double[] hasilPairwiseComparisonKriteria) {
        this.hasilPairwiseComparisonKriteria = hasilPairwiseComparisonKriteria;
    }

    
      
}
