/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.calculate;

import com.farindika.calculate.util.CalculateUtil;

/**
 *
 * ANP = Analytical Network Process Weighting Criteria
 */
public class ANP implements Calculate {

    /**
     * data skor masukan
     */
    private int[] nodeSeparator;
    private double[][] skorNode;
    private double[][] skorKlaster;
    /**
     * data hasil proses kalkulasi ANP
     */
    public double[][] unWeightSuperMatrix;
    public double[][] prioritasKlasterMatrix;
    public double[][] weightSuperMatrix;
    public double[][] weightSuperMatrixStokastik;
    public double[][] limitSuperMatrix;
    public double[] resultLimitNormalisasi;

    public void calculate() {
        calculateUnWeightSuperMatix();
        calculatePrioritasKlasterMatrix();
        calculateWeightSuperMatrix();
        weightSuperMatrixStokastik = CalculateUtil.normalisasi(weightSuperMatrix);
        calculateLimitSuperMatrix();

    }

    private void calculateUnWeightSuperMatix() {
        unWeightSuperMatrix = new double[skorNode.length][skorNode.length];
        int firstKlasterIndex = 0;
        for (int i = 0; i < nodeSeparator.length; i++) {
            for (int col = 0; col < skorNode.length; col++) {
                double[][] pairwise = new double[nodeSeparator[i]][nodeSeparator[i]];
                for (int prRow = 0; prRow < pairwise.length; prRow++) {
                    for (int prCol = 0; prCol < pairwise.length; prCol++) {
                        if (skorNode[firstKlasterIndex + prCol][col] == 0) {
                            pairwise[prRow][prCol] = 0;
                        } else {
                            pairwise[prRow][prCol] = skorNode[firstKlasterIndex + prRow][col] / skorNode[firstKlasterIndex + prCol][col];
                        }
                    }
                }

                pairwise = CalculateUtil.normalisasi(pairwise);
                double[] prioritas = CalculateUtil.average(pairwise);

                for (int uwsmRow = 0; uwsmRow < pairwise.length; uwsmRow++) {
                    unWeightSuperMatrix[firstKlasterIndex + uwsmRow][col] = CalculateUtil.round(prioritas[uwsmRow], 2);
                }
            }
            firstKlasterIndex += nodeSeparator[i];
        }
        System.out.println("+++++++++++++++THIS IS UNWEIGHTED SUPERMATRIX+++++++++++++++++++++++++");
        CalculateUtil.showArray(unWeightSuperMatrix);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("");
    }

    private void calculatePrioritasKlasterMatrix() {
        prioritasKlasterMatrix = new double[skorKlaster.length][skorKlaster.length];
        for (int col = 0; col < skorKlaster.length; col++) {
            double[][] pairwise = new double[skorKlaster.length][skorKlaster.length];
            /*
             * memotong matriks pairwise klaster
             */
            for (int prRow = 0; prRow < skorKlaster.length; prRow++) {
                for (int prCol = 0; prCol < skorKlaster.length; prCol++) {
                    if (skorKlaster[prCol][col] == 0) {
                        pairwise[prRow][prCol] = 0;
                    } else {
                        pairwise[prRow][prCol] = skorKlaster[prRow][col] / skorKlaster[prCol][col];
                    }
                }
            }

            /*
             * calculate normalisasi klaster pairwise dan mencari prioritas
             */
            pairwise = CalculateUtil.normalisasi(pairwise);
            double[] prioritas = CalculateUtil.average(pairwise);

            for (int j = 0; j < skorKlaster.length; j++) {
                prioritasKlasterMatrix[j][col] = CalculateUtil.round(prioritas[j], 2);
            }
        }
        /*
         * Show Prioritas dalam matrix klaster
         */
        CalculateUtil.showArray(prioritasKlasterMatrix);

    }

    private void calculateWeightSuperMatrix() {
        weightSuperMatrix = new double[unWeightSuperMatrix.length][unWeightSuperMatrix.length];
        int firstKlasterRowIndex = 0;
        for (int pkmRow = 0; pkmRow < prioritasKlasterMatrix.length; pkmRow++) {
            int firstKlasterColIndex = 0;
            for (int pkmCol = 0; pkmCol < prioritasKlasterMatrix.length; pkmCol++) {
                for (int wsmRow = firstKlasterRowIndex; wsmRow < firstKlasterRowIndex + nodeSeparator[pkmRow]; wsmRow++) {
                    for (int wsmCol = firstKlasterColIndex; wsmCol < firstKlasterColIndex + nodeSeparator[pkmCol]; wsmCol++) {
                        weightSuperMatrix[wsmRow][wsmCol] = prioritasKlasterMatrix[pkmRow][pkmCol] * unWeightSuperMatrix[wsmRow][wsmCol];
                        weightSuperMatrix[wsmRow][wsmCol] = CalculateUtil.round(weightSuperMatrix[wsmRow][wsmCol], 2);
                    }
                }

                firstKlasterColIndex += nodeSeparator[pkmCol];
            }

            firstKlasterRowIndex += nodeSeparator[pkmRow];
        }

        System.out.println("+++++++++++++++THIS IS WEIGHTED SUPERMATRIX+++++++++++++++++++++++++");
        CalculateUtil.showArray(weightSuperMatrix);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    public void calculateLimitSuperMatrix() {
        limitSuperMatrix = new double[weightSuperMatrixStokastik.length][weightSuperMatrixStokastik.length];
        boolean isFirst = true;

        int count = 0;
        while (true) {
            limitSuperMatrix = CalculateUtil.multiplication(isFirst ? weightSuperMatrixStokastik : limitSuperMatrix, weightSuperMatrixStokastik);
            limitSuperMatrix = CalculateUtil.normalisasi(limitSuperMatrix);

            boolean isSame = true;
            for (int row = 0; row < limitSuperMatrix.length; row++) {
                for (int col = 0; col < limitSuperMatrix.length; col++) {
                    if (limitSuperMatrix[row][0] != limitSuperMatrix[row][col]) {
                        isSame = false;
                        break;
                    }
                }
                if (!isSame) {
                    break;
                }
            }

            if (isSame) {
                break;
            }
            ++count;
            isFirst = false;
        }

        System.out.println("+++++++++++++++THIS IS LIMIT SUPERMATRIX+++++++++++++++++++++++++");
        CalculateUtil.showArray(limitSuperMatrix);
        System.out.println("count loop = " + count);
        System.out.println("+++ ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    public double[][] getLimitSuperMatrix() {
        return limitSuperMatrix;
    }

    public double[] getResultLimitNormalisasi() {
        return resultLimitNormalisasi;
    }

    public double[][] getUnWeightSuperMatrix() {
        return unWeightSuperMatrix;
    }

    public double[][] getWeightSuperMatrix() {
        return weightSuperMatrix;
    }

    public double[][] getWeightSuperMatrixStokastik() {
        return weightSuperMatrixStokastik;
    }

    public int[] getNodeSeparator() {
        return nodeSeparator;
    }

    public void setNodeSeparator(int[] nodeSeparator) {
        this.nodeSeparator = nodeSeparator;
    }

    public double[][] getSkorKlaster() {
        return skorKlaster;
    }

    public void setSkorKlaster(double[][] skorKlaster) {
        this.skorKlaster = skorKlaster;
    }

    public double[][] getSkorNode() {
        return skorNode;
    }

    public void setSkorNode(double[][] skorNode) {
        this.skorNode = skorNode;
    }

    public double[][] getPrioritasKlasterMatrix() {
        return prioritasKlasterMatrix;
    }
}
