/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.calculate;
import com.farindika.calculate.util.CalculateUtil;
import java.math.*;
/**
 *
 * @author Ultra
 */
public class TOPSIS implements Calculate 
{
    double [] bobotKriteria;
    double [][] nilaiPenyedia;
    int jmlAlternatif, jmlKriteria;
    
    double [][] normalizedDecisionMatrix;
    double [][] weightedNormalizedDecisionMatrix;
    double [] positifIdealSolution;
    double [] negatifIdealSolution;
    double []distanceAlternativeToPositifIdealSolution;
    double []distanceAlternativeToNegatifIdealSolution;
    double [] closenesCoefficient;
    double [] sortClosenesCoefficient;
    
    
    public void calculate() 
    {
       calculateNormalizedDecisionMatrix();
       calculateWeightedNormalizedDecisionMatrix();
       calculatePositifIdealSolution();
       calculateNegatifIdealSolution();
       calculateDistanceAlternativeToPositifIdealSolution();
       calculateDistanceAlternativeToNegatifIdealSolution();
       calculateClosenessCoefficient();
       
    }
    
     public double [][] calculateNormalizedDecisionMatrix()
    {
        double [] sumPowSqrt = new double[jmlKriteria];
        normalizedDecisionMatrix = new double[jmlAlternatif][jmlKriteria];
       
        /*
         * Calculate Normalize Decision Matrix
         */
        for(int col = 0; col<jmlKriteria;col++)
        {
            
            double sumPow=0.d;
            for(int row = 0;row<jmlAlternatif;row++)
            {
                sumPow = sumPow + Math.pow(nilaiPenyedia[row][col],2);
            }
            sumPowSqrt[col]= Math.sqrt(sumPow);
            for(int row= 0;row<nilaiPenyedia.length;row++)
            {
                normalizedDecisionMatrix[row][col]=nilaiPenyedia[row][col]/sumPowSqrt[col];
            }
        }
        /*
         * Show normalize decision matrix
         */
        System.out.println("+++++++++++++++++++NORMALIZED DECISION MATRIX++++++++++++++++++");
        for(int row = 0; row<jmlAlternatif;row++)
        {
            
            for(int col = 0;col<jmlKriteria;col++)
            {
                System.out.print("\t" + CalculateUtil.round(normalizedDecisionMatrix[row][col],4));
            }
            System.out.println(" ");
        }
        System.out.println("+++++++++++++++++++NORMALIZED DECISION MATRIX++++++++++++++++++");
        return normalizedDecisionMatrix;
    }
    
    public double [][] calculateWeightedNormalizedDecisionMatrix()
    {
        weightedNormalizedDecisionMatrix = new double[jmlAlternatif][jmlKriteria]; 
        for(int col = 0; col<jmlKriteria;col++)
        {
            for(int row = 0;row<jmlAlternatif;row++)
            {
                weightedNormalizedDecisionMatrix[row][col] = normalizedDecisionMatrix[row][col] * bobotKriteria[col];
            }
       }
        System.out.println("+++++++++++++++++++WEIGHTED NORMALIZED DECISION MATRIX++++++++++++++++++");
        for(int i = 0; i<jmlAlternatif;i++)
        {
            for(int j = 0; j<jmlKriteria;j++)
            {
                System.out.print("\t" +CalculateUtil.round(weightedNormalizedDecisionMatrix[i][j],4));
                
            }
            System.out.println("");
        }
        System.out.println("+++++++++++++++++++WEIGHTED NORMALIZED DECISION MATRIX++++++++++++++++++");
        return weightedNormalizedDecisionMatrix;
        
    }
    
    public double [] calculatePositifIdealSolution()
    {
       positifIdealSolution = new double[jmlKriteria];
       double max = 0d;
       for(int col = 0; col<jmlKriteria;col++)
        {
            max = 0d;
            for(int row = 0;row<jmlAlternatif;row++)
            {
                
                if(weightedNormalizedDecisionMatrix[row][col]>max)
                {
                    max=weightedNormalizedDecisionMatrix[row][col];
                }
                positifIdealSolution[col]=max;    
            }
           
       }
        System.out.println("+++++++++++++++++++POSITIF IDEAL SOLUTION++++++++++++++++++");
        for(int i = 0; i<jmlKriteria;i++)
        {
            System.out.print("\t" + CalculateUtil.round(positifIdealSolution[i],4));
        }
        System.out.println("");
        System.out.println("+++++++++++++++++++POSITIF IDEAL SOLUTION++++++++++++++++++");
           return positifIdealSolution; 
    }
    
    public double [] calculateNegatifIdealSolution()
    {
        negatifIdealSolution = new double[jmlKriteria];
       double min = 0d;
       for(int col = 0; col<jmlKriteria;col++)
        {
           min = 1;
            for(int row = 0;row<jmlAlternatif;row++)
            {
                
                if(weightedNormalizedDecisionMatrix[row][col]<min)
                {
                    min=weightedNormalizedDecisionMatrix[row][col];
                }
                negatifIdealSolution[col]=min;    
            }
           
       }
        System.out.println("+++++++++++++++++++NEGATIF IDEAL SOLUTION++++++++++++++++++");
        for(int i = 0; i<jmlKriteria;i++)
        {
            System.out.print("\t" + CalculateUtil.round(negatifIdealSolution[i],4));
        }
        System.out.println("");
        System.out.println("+++++++++++++++++++NEGATIF IDEAL SOLUTION++++++++++++++++++");
           return negatifIdealSolution; 
    }
    
    public double []calculateDistanceAlternativeToPositifIdealSolution()
    {
        distanceAlternativeToPositifIdealSolution= new double[jmlAlternatif];
        double [] temp = new double[jmlAlternatif];
        
        for(int i = 0; i<jmlAlternatif;i++)
        {
            temp[i]=0d;
        }
        
        for(int row = 0; row<jmlAlternatif;row++)
        {
            for(int col = 0;col<jmlKriteria;col++)
            {
                temp[row] = temp[row] + Math.pow((weightedNormalizedDecisionMatrix[row][col]- positifIdealSolution[col]), 2);
                
            }
            
            distanceAlternativeToPositifIdealSolution[row]= Math.sqrt(temp[row]);
       }
        System.out.println("+++++++++++++++++++ALTERNATIF TO POSITIF IDEAL SOLUTION++++++++++++++++++");
        for(int i = 0; i<jmlAlternatif;i++)
        {
            System.out.print("\t" + CalculateUtil.round(distanceAlternativeToPositifIdealSolution[i],4));
        }
        System.out.println("");
        System.out.println("+++++++++++++++++++ALTERNATIF TO POSITIF IDEAL SOLUTION++++++++++++++++++");
       return distanceAlternativeToPositifIdealSolution;
    }
    
    public double [] calculateDistanceAlternativeToNegatifIdealSolution()
    {
        distanceAlternativeToNegatifIdealSolution= new double[jmlAlternatif];
        double [] temp = new double[jmlAlternatif];
        
        for(int i = 0; i<jmlAlternatif;i++)
        {
            temp[i]=0d;
        }
        for(int row = 0; row<jmlAlternatif;row++)
        {
            for(int col = 0;col<jmlKriteria;col++)
            {
                temp[row] = temp[row] + Math.pow((weightedNormalizedDecisionMatrix[row][col]- negatifIdealSolution[col]), 2);
                
            }
            
            distanceAlternativeToNegatifIdealSolution[row]= Math.sqrt(temp[row]);
       }
        System.out.println("+++++++++++++++++++ALTERNATIF TO NEGATIF IDEAL SOLUTION++++++++++++++++++");
        for(int i = 0; i<jmlAlternatif;i++)
        {
            System.out.print("\t" + CalculateUtil.round(distanceAlternativeToNegatifIdealSolution[i],4));
        }
        System.out.println("");
        System.out.println("+++++++++++++++++++ALTERNATIF TO NEGATIF IDEAL SOLUTION++++++++++++++++++");
       return distanceAlternativeToNegatifIdealSolution;
    }
    
    public double [] calculateClosenessCoefficient()
    {
        closenesCoefficient = new double[jmlAlternatif];
         for(int i = 0; i<jmlAlternatif;i++)
        {
            closenesCoefficient[i] = distanceAlternativeToNegatifIdealSolution[i]/(distanceAlternativeToNegatifIdealSolution[i] + distanceAlternativeToPositifIdealSolution[i]);
        }
        System.out.println("+++++++++++++++++++CLOSENESS COEFFICIENT++++++++++++++++++");
         for(int i = 0; i<jmlAlternatif;i++)
        {
            System.out.print("\t" + CalculateUtil.round(closenesCoefficient[i],4));
        }
        System.out.println("");
        System.out.println("+++++++++++++++++++CLOSENESS COEFFICIENT++++++++++++++++++");
        return closenesCoefficient;
    }
    
    public double [] calculateSortClosenessCoefficient()
    {
        sortClosenesCoefficient = new double[jmlAlternatif];
       
        return sortClosenesCoefficient;
    }
    
    public double[] getNegatifIdealSolution() {
        return negatifIdealSolution;
    }

    public double[][] getNormalizedDecisionMatrix() {
        return normalizedDecisionMatrix;
    }

    public double[] getPositifIdealSolution() {
        return positifIdealSolution;
    }

    public double[] getClosenesCoefficient() {
        return closenesCoefficient;
    }

    public double[] getSortClosenesCoefficient() {
        return sortClosenesCoefficient;
    }

    public double[][] getWeightedNormalizedDecisionMatrix() {
        return weightedNormalizedDecisionMatrix;
    }
    
  

    public int getJmlAlternatif() {
        return jmlAlternatif;
    }

    public void setJmlAlternatif(int jmlAlternatif) {
        this.jmlAlternatif = jmlAlternatif;
    }

    public int getJmlKriteria() {
        return jmlKriteria;
    }

    public void setJmlKriteria(int jmlKriteria) {
        this.jmlKriteria = jmlKriteria;
    }

    public double[][] getNilaiPenyedia() {
        return nilaiPenyedia;
    }

    public void setNilaiPenyedia(double[][] nilaiPenyedia) {
        this.nilaiPenyedia = nilaiPenyedia;
    }

    public double[] getBobotKriteria() {
        return bobotKriteria;
    }

    public void setBobotKriteria(double[] bobotKriteria) {
        this.bobotKriteria = bobotKriteria;
    }

    public double[] getDistanceAlternativeToNegatifIdealSolution() {
        return distanceAlternativeToNegatifIdealSolution;
    }

    public void setDistanceAlternativeToNegatifIdealSolution(double[] distanceAlternativeToNegatifIdealSolution) {
        this.distanceAlternativeToNegatifIdealSolution = distanceAlternativeToNegatifIdealSolution;
    }

    public double[] getDistanceAlternativeToPositifIdealSolution() {
        return distanceAlternativeToPositifIdealSolution;
    }

    public void setDistanceAlternativeToPositifIdealSolution(double[] distanceAlternativeToPositifIdealSolution) {
        this.distanceAlternativeToPositifIdealSolution = distanceAlternativeToPositifIdealSolution;
    }

    public void setClosenesCoefficient(double[] closenesCoefficient) {
        this.closenesCoefficient = closenesCoefficient;
    }

    public void setNegatifIdealSolution(double[] negatifIdealSolution) {
        this.negatifIdealSolution = negatifIdealSolution;
    }

    public void setNormalizedDecisionMatrix(double[][] normalizedDecisionMatrix) {
        this.normalizedDecisionMatrix = normalizedDecisionMatrix;
    }

    public void setPositifIdealSolution(double[] positifIdealSolution) {
        this.positifIdealSolution = positifIdealSolution;
    }

    public void setSortClosenesCoefficient(double[] sortClosenesCoefficient) {
        this.sortClosenesCoefficient = sortClosenesCoefficient;
    }

    public void setWeightedNormalizedDecisionMatrix(double[][] weightedNormalizedDecisionMatrix) {
        this.weightedNormalizedDecisionMatrix = weightedNormalizedDecisionMatrix;
    }
      
    
}
