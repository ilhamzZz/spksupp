/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.report.model;

/**
 *
 * @author Ultra
 */
public class ReportUrutanPenyedia 
{
    public int urutan;
    public String namaPenyedia;
    public double nilaiCC;

    public String getNamaPenyedia() {
        return namaPenyedia;
    }

    public void setNamaPenyedia(String namaPenyedia) {
        this.namaPenyedia = namaPenyedia;
    }

    public double getNilaiCC() {
        return nilaiCC;
    }

    public void setNilaiCC(double nilaiCC) {
        this.nilaiCC = nilaiCC;
    }

    public int getUrutan() {
        return urutan;
    }

    public void setUrutan(int urutan) {
        this.urutan = urutan;
    }
    
    
    
}
