package com.farindika;

//import com.farindika.db.HibernateUtil;
import com.farindika.calculate.ANP;
import com.farindika.calculate.BOBOTANP;
import com.farindika.calculate.TOPSIS;
import com.farindika.view.MainFrame;
import java.awt.Frame;
import javax.swing.JFrame;
import javax.swing.UIManager;
//import javax.swing.UIManager;

/**
 * Hello world!
 *
 */
public class Main {

    private static MainFrame mainFrame;
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
//            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {
                mainFrame = new MainFrame();
                mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                mainFrame.setVisible(true);
            }
        });
    }
    
    
//    public static void main(String[] args) 
//    {
//        String input;
//      double [][]skor = 
//      {
//          {0,0,0,5,4,4,4},
//          {0,0,0,3,5,5,4},
//          {0,0,0,3,5,4,3},
//          {5,6,7,0,0,8,7},
//          {8,9,8,0,0,0,0},
//          {6,9,6,0,0,0,8},
//          {7,6,5,0,0,0,0}
//      };
//      int [] nodeSeparator = {3,2,2};
//      
//      double [][] skorKlaster = 
//      {
//          {0,7,9},
//          {9,0,8},
//          {8,0,9}
//      };
//      double [][] dataWeightedAlternatif = 
//      {
//          {0.45,0.29,0.16,0.13},
//          {0.27,0.36,0.20,0.13},
//          {0.27,0.36,0.16,0.09}
//      };
//      
//      double [] bobotNormalisasiLimitKriteria = 
//      {0.34,0.24,0.25,0.17};
//      
////      ANP anp = new ANP();
////      anp.setSkorNode(skor);
////      anp.setNodeSeparator(nodeSeparator);
////      anp.setSkorKlaster(skorKlaster);
////      anp.calculate();
//
//       TOPSIS topsis = new TOPSIS();
//       topsis.setJmlAlternatif(3);
//       topsis.setJmlKriteria(4);
//       topsis.setUnWeightSuperMatrixAlternatif(dataWeightedAlternatif);
//       topsis.setWeightLimitKriteriaNormalisasi(bobotNormalisasiLimitKriteria);
//       topsis.calculate();
//      
//        double [][] nilaiKepentingan =
//        {
//            {1, 5, 5, 3, 1, 1, 5, 3},
//            {0.2, 1, 1, 0.5, 0.2, 0.2, 1, 0.5},
//            {0.2, 1, 1, 0.5, 0.2, 0.2, 1, 0.5},
//            {0.3333, 2, 2, 1, 0.3333, 0.3333, 2, 1},
//            {1, 5, 5, 3, 1, 1, 5, 3},
//            {1, 5, 5, 3, 1, 1, 5, 3},
//            {0.2, 1, 1, 0.5, 0.2, 0.2, 1, 0.5},
//            {0.3333, 2, 2, 1, 0.3333, 0.3333, 2, 1}
//       };
//        
//        double [][] prioritasPengaruhKriteria =
//        {
//            {0.0820, 0, 0, 0.4286, 0, 0, 0, 0},
//            {0.4486, 1, 0.8333, 0.4286, 0, 0.6479, 0, 0},
//            {0.2347, 0, 0.1667, 0, 0, 0.2299, 0, 0},
//            {0, 0, 0, 0.1429, 0, 0, 0, 0},
//            {0, 0, 0, 0, 1, 0, 0, 0},
//            {0, 0, 0, 0, 0, 0.1222, 0, 0},
//            {0.2347, 0, 0, 0, 0, 0, 1, 0},
//            {0, 0, 0, 0, 0, 0, 0, 1}
//
//        };
//        
//        int [][] nilaiPenyedia =
//        {
//            {19, 19, 24, 20, 23, 24, 19, 21},
//            {21, 21, 16, 22, 20, 22, 16, 24},
//            {23, 21, 18, 24, 19, 18, 16, 24},
//            {18, 25, 22, 20, 20, 19, 16, 20},
//            {23, 25, 18, 19, 22, 20, 15, 25}
//  
//        };
//        int jmlAlternatif = 5;
//        int jmlKriteria = 8;
//        
//        BOBOTANP anp = new BOBOTANP();
//        anp.setNilaiKepentinganKriteria(nilaiKepentingan);
//        anp.setBobotRelatifinterdependence(prioritasPengaruhKriteria);
//        anp.calculate();
//        
//        TOPSIS topsis = new TOPSIS();
//        topsis.setJmlAlternatif(jmlAlternatif);
//        topsis.setJmlKriteria(jmlKriteria);
//        topsis.setNilaiPenyedia(nilaiPenyedia);
//        topsis.setBobotKriteria(anp.bobotRelatifKriteria);
//        topsis.calculate();
//        
//        
//    }
    public static MainFrame getMainFrame()
    {
        return mainFrame;
    }
}
