/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.view.dialog;

import com.farindika.controller.NodeController;
import com.farindika.db.entity.Node;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author LENOVO
 */
public class NodeDialog extends javax.swing.JDialog {

    /**
     * Creates new form NodeAddEditDialog
     */
    public NodeDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        namaKlasterCombo = new javax.swing.JComboBox();
        namaNodeText = new javax.swing.JTextField();
        nodeAlternatifCombo = new javax.swing.JComboBox();
        kodeNodeText = new javax.swing.JTextField();
        ketNodeText = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        simpanButton = new javax.swing.JButton();
        batalButton = new javax.swing.JButton();

        jLabel3.setText("Nama Klaster    :");

        jLabel4.setText("Nama Node       :");

        jLabel5.setText("Node Alternatif :");

        jLabel6.setText("Kode Node        :");

        jLabel7.setText("Keterangan      :");

        namaKlasterCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        namaKlasterCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                namaKlasterComboItemStateChanged(evt);
            }
        });
        namaKlasterCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaKlasterComboActionPerformed(evt);
            }
        });

        namaNodeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaNodeTextActionPerformed(evt);
            }
        });

        nodeAlternatifCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        nodeAlternatifCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nodeAlternatifComboItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(namaKlasterCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(namaNodeText)
                    .addComponent(nodeAlternatifCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(kodeNodeText)
                    .addComponent(ketNodeText))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(namaKlasterCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(namaNodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(nodeAlternatifCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(kodeNodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(ketNodeText, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        simpanButton.setText("Simpan");
        simpanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanButtonActionPerformed(evt);
            }
        });

        batalButton.setText("Batal");
        batalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batalButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(129, 129, 129)
                .addComponent(simpanButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(batalButton)
                .addContainerGap(121, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(simpanButton)
                    .addComponent(batalButton))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void namaNodeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaNodeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaNodeTextActionPerformed

    private void batalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batalButtonActionPerformed
        // TODO add your handling code here:
        setVisible(false);
    }//GEN-LAST:event_batalButtonActionPerformed

    private void simpanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanButtonActionPerformed
        // TODO add your handling code here:
        Object selectedItem = namaKlasterCombo.getSelectedItem();
        if("alternatif".equalsIgnoreCase(selectedItem + ""))
        {
            packDataAlternatif();
        setVisible(false);
        save = true;
        }
        
        else
        {
          packData();
        setVisible(false);
        save = true;
        }
    }//GEN-LAST:event_simpanButtonActionPerformed

    private void namaKlasterComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaKlasterComboActionPerformed
    
    }//GEN-LAST:event_namaKlasterComboActionPerformed

    private void namaKlasterComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_namaKlasterComboItemStateChanged
        Object selectedItem = namaKlasterCombo.getSelectedItem();
        if("alternatif".equalsIgnoreCase(selectedItem + ""))
        {
            namaNodeText.setEditable(false);
            nodeAlternatifCombo.setEnabled(true);
             nodeAlternatifCombo.setModel(new DefaultComboBoxModel(controller.findPenyediaAllArray()));
            nodeAlternatifCombo.setSelectedIndex(-1);
                       
        }
        else
        {
            namaNodeText.setEditable(true);
            nodeAlternatifCombo.setEnabled(false);
        }
            
            
    }//GEN-LAST:event_namaKlasterComboItemStateChanged

    private void nodeAlternatifComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nodeAlternatifComboItemStateChanged
        Object selectedItem = nodeAlternatifCombo.getSelectedItem();
  
    }//GEN-LAST:event_nodeAlternatifComboItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NodeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NodeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NodeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NodeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                NodeDialog dialog = new NodeDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton batalButton;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField ketNodeText;
    private javax.swing.JTextField kodeNodeText;
    private javax.swing.JComboBox namaKlasterCombo;
    private javax.swing.JTextField namaNodeText;
    private javax.swing.JComboBox nodeAlternatifCombo;
    private javax.swing.JButton simpanButton;
    // End of variables declaration//GEN-END:variables
    private NodeController controller;
    private Node node;
    private boolean save = false;

    public NodeController getController() {
        return controller;
    }

    public void setController(NodeController controller) {
        this.controller = controller;
    }

    public Node getNode() {
        packData();
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
        unpackData();
    }

    public boolean isSave() {
        return save;
    }

    @Override
    public void setVisible(boolean b) 
    {
        if (b) 
        {
            namaKlasterCombo.setModel(new DefaultComboBoxModel(controller.findKlasterAllArray()));
            namaKlasterCombo.setSelectedIndex(-1);
            nodeAlternatifCombo.setModel(new DefaultComboBoxModel(controller.findPenyediaAllArray()));
            nodeAlternatifCombo.setSelectedIndex(-1);
            unpackData();
        }
        super.setVisible(b);
    }

    public void enableInput(boolean enable) 
    {
       namaNodeText.setEditable(enable);
        namaKlasterCombo.setEnabled(enable);
        nodeAlternatifCombo.setEnabled(enable);
        ketNodeText.setEditable(enable);
        kodeNodeText.setEditable(enable);
        simpanButton.setVisible(enable);
    }

    public void packData() 
    {
        if (node == null) 
        {
            node = new Node();
        }
        node.setKlaster(controller.findKlasterFromComboIndex(namaKlasterCombo.getSelectedIndex()));
        node.setNama(namaNodeText.getText());
        node.setKode(kodeNodeText.getText());
        node.setKeterangan(ketNodeText.getText());

    }
    
      public void packDataAlternatif() 
    {
        Object selectedItem = nodeAlternatifCombo.getSelectedItem();
        if (node == null) 
        {
            node = new Node();
        }
        
        node.setKlaster(controller.findKlasterFromComboIndex(namaKlasterCombo.getSelectedIndex()));
        node.setKode(kodeNodeText.getText());
        node.setNama(selectedItem.toString());
        node.setKeterangan(ketNodeText.getText());
        node.setPenyedia(controller.findPenyediaFromComboIndex(nodeAlternatifCombo.getSelectedIndex()));

    }

    public void unpackData() 
    {
        if (node == null) 
        {
            return;
        }
        namaNodeText.setText(node.getNama());
        namaKlasterCombo.setSelectedItem(node.getKlaster().getNama());
        ketNodeText.setText(node.getKeterangan());
        kodeNodeText.setText(node.getKode());
        nodeAlternatifCombo.setSelectedItem("");
        if(node.getPenyedia()!=null)
            nodeAlternatifCombo.setSelectedItem(node.getPenyedia().getNama());
    }

}
