/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.view.panel;

import com.farindika.calculate.BOBOTANP;
import com.farindika.calculate.util.CalculateUtil;
import com.farindika.controller.MatriksPerbandinganPengaruhKriteriaController;
import com.farindika.controller.PenilaianRelasiKriteriaController;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganPengaruhKriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import com.farindika.view.util.CostumTableModel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Ultra
 */
public class MatriksPerbandinganPengaruhKriteriaPanel extends javax.swing.JPanel {

    /**
     * Creates new form MatriksPerbandinganPengaruhKriteriaPanel
     */
    public MatriksPerbandinganPengaruhKriteriaPanel() {
        initComponents();
        controller = new MatriksPerbandinganPengaruhKriteriaController();
        controllerPenilaianRelasiKriteria = new PenilaianRelasiKriteriaController();
        berkasCombo.setModel(new DefaultComboBoxModel(controllerPenilaianRelasiKriteria.findBerkasAllArray()));
        berkasCombo.setSelectedIndex(-1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jLabel2 = new javax.swing.JLabel();
        berkasCombo = new javax.swing.JComboBox();
        simpanButton = new javax.swing.JButton();
        hapusButton = new javax.swing.JButton();
        panelInputKdipengaruhi = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        kDipengaruhiCombo = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        kriteria1Combo = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        nilaiCombo = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        kriteria2Combo = new javax.swing.JComboBox();
        tambahButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        lambdaMaxLabel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        cILabel = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        cRLabel = new javax.swing.JLabel();
        konsistensiLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelMatriks = new javax.swing.JTable();
        periksaButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jToolBar1.setBackground(new java.awt.Color(255, 255, 255));
        jToolBar1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setText("Data Nilai Pengaruh Relasi");
        jToolBar1.add(jLabel1);
        jToolBar1.add(jSeparator1);

        jLabel2.setText("Berkas");
        jToolBar1.add(jLabel2);

        berkasCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        berkasCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                berkasComboItemStateChanged(evt);
            }
        });
        jToolBar1.add(berkasCombo);

        simpanButton.setBackground(new java.awt.Color(255, 255, 255));
        simpanButton.setText("Simpan");
        simpanButton.setToolTipText("Menyimpan atau Memperbaharui Data Perbandingan Pengaruh Kriteria");
        simpanButton.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        simpanButton.setFocusable(false);
        simpanButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        simpanButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        simpanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(simpanButton);

        hapusButton.setBackground(new java.awt.Color(255, 255, 255));
        hapusButton.setText("Hapus");
        hapusButton.setToolTipText("Menghapusa Data Perbandingan Pengaruh Kriteria");
        hapusButton.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        hapusButton.setFocusable(false);
        hapusButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hapusButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        hapusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(hapusButton);

        panelInputKdipengaruhi.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Ebrima", 1, 14)); // NOI18N
        jLabel3.setText("Kriteria Dipengaruhi");

        kDipengaruhiCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        kDipengaruhiCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                kDipengaruhiComboItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelInputKdipengaruhiLayout = new javax.swing.GroupLayout(panelInputKdipengaruhi);
        panelInputKdipengaruhi.setLayout(panelInputKdipengaruhiLayout);
        panelInputKdipengaruhiLayout.setHorizontalGroup(
            panelInputKdipengaruhiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInputKdipengaruhiLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(kDipengaruhiCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelInputKdipengaruhiLayout.setVerticalGroup(
            panelInputKdipengaruhiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInputKdipengaruhiLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kDipengaruhiCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Ebrima", 1, 14)); // NOI18N
        jLabel4.setText("Kriteria Mempengaruhi");

        jLabel5.setText("Kriteria 1");

        kriteria1Combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Nilai Pengaruh");

        nilaiCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "9", "8", "7", "6", "5", "4", "3", "2", "1", "0.5", "0.333333", "0.25", "0.2", "0.1666667", "0.142857", "0.125", "0.111111" }));

        jLabel7.setText("Kriteria 2");

        kriteria2Combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        tambahButton.setBackground(new java.awt.Color(255, 255, 255));
        tambahButton.setText("Tambah");
        tambahButton.setToolTipText("Memasukkan Nilai Pengaruh Antar Kriteria Ke Dalam Matriks Perbandingan Berpasangan");
        tambahButton.setBorder(null);
        tambahButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                .addGap(534, 534, 534))
            .addComponent(kriteria2Combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nilaiCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(kriteria1Combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(tambahButton, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kriteria1Combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nilaiCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kriteria2Combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tambahButton, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE))
        );

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Matriks Perbandingan Berpasangan Interdependence");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setText("Lambda Max");

        lambdaMaxLabel.setText("Nilai Lamda");

        jLabel11.setText("CI");

        cILabel.setText("Nilai");

        jLabel13.setText("CR");

        cRLabel.setText("Nilai");

        konsistensiLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        konsistensiLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        konsistensiLabel.setText("Konsistensi Check");

        tabelMatriks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelMatriks);

        periksaButton.setText("Periksa");
        periksaButton.setToolTipText("Memeriksa Konsistensi Dari Nilai Perbandingan Berpasangan");
        periksaButton.setBorder(null);
        periksaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                periksaButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(konsistensiLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(periksaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel13)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cRLabel))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cILabel))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addGap(18, 18, 18)
                                    .addComponent(lambdaMaxLabel))))
                        .addGap(14, 14, 14)))
                .addGap(4, 4, 4))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(periksaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lambdaMaxLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cILabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cRLabel))
                .addGap(18, 18, 18)
                .addComponent(konsistensiLabel)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelInputKdipengaruhi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelInputKdipengaruhi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void berkasComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_berkasComboItemStateChanged
        // TODO add your handling code here:
        reModelComboBerkas() ;
    }//GEN-LAST:event_berkasComboItemStateChanged

    private void kDipengaruhiComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_kDipengaruhiComboItemStateChanged
        // TODO add your handling code here:
        reModelComboKDipengaruhi();
        reModelMatriks();
    }//GEN-LAST:event_kDipengaruhiComboItemStateChanged

    private void simpanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanButtonActionPerformed
        // TODO add your handling code here:
        if(berkasCombo.getSelectedIndex()==-1)
        {
            JOptionPane.showMessageDialog(null, "Berkas belum dipilih", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            save();
        }
    }//GEN-LAST:event_simpanButtonActionPerformed

    private void tambahButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tambahButtonActionPerformed
        // TODO add your handling code here:
        if(kriteria1Combo.getSelectedIndex()==-1||kriteria2Combo.getSelectedIndex()==-1)
        {
            JOptionPane.showMessageDialog(null, "kriteria yang dibandingkan belum dipilih", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            int indeksKriteria1 = kriteria1Combo.getSelectedIndex();
            int indeksKriteria2 = kriteria2Combo.getSelectedIndex();
            Double nilai = Double.parseDouble(nilaiCombo.getSelectedItem()+"");

            tabelMatriks.setValueAt(nilai, indeksKriteria1, indeksKriteria2+1);
            tabelMatriks.setValueAt(1/nilai, indeksKriteria2, indeksKriteria1+1);
        }
            kriteria1Combo.setSelectedIndex(-1);
            kriteria2Combo.setSelectedIndex(-1);
            
    }//GEN-LAST:event_tambahButtonActionPerformed

    private void periksaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_periksaButtonActionPerformed
        // TODO add your handling code here:
        konsistensiCheck();
    }//GEN-LAST:event_periksaButtonActionPerformed

    private void hapusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusButtonActionPerformed
        // TODO add your handling code here:
       if(berkasCombo.getSelectedIndex()==-1)
        {
            JOptionPane.showMessageDialog(null, "Berkas belum dipilih", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            int retValue = JOptionPane.showConfirmDialog(null, "Apakah anda yakin?", "Konfirmasi Penghapusan" , JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(retValue == 0)
            {
                delete();
            }
        }
    }//GEN-LAST:event_hapusButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox berkasCombo;
    private javax.swing.JLabel cILabel;
    private javax.swing.JLabel cRLabel;
    private javax.swing.JButton hapusButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox kDipengaruhiCombo;
    private javax.swing.JLabel konsistensiLabel;
    private javax.swing.JComboBox kriteria1Combo;
    private javax.swing.JComboBox kriteria2Combo;
    private javax.swing.JLabel lambdaMaxLabel;
    private javax.swing.JComboBox nilaiCombo;
    private javax.swing.JPanel panelInputKdipengaruhi;
    private javax.swing.JButton periksaButton;
    private javax.swing.JButton simpanButton;
    private javax.swing.JTable tabelMatriks;
    private javax.swing.JButton tambahButton;
    // End of variables declaration//GEN-END:variables
    private MatriksPerbandinganPengaruhKriteriaController controller;
    private PenilaianRelasiKriteriaController controllerPenilaianRelasiKriteria;
    private MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria;
    private boolean save = false;
    private String[] columnNames;
    private List<Kriteria> kriterias;
    private List<PenilaianRelasiKriteria> penilaianRelasiKriterias;
    private List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias;
    private boolean isSaveNew = true;
    
    /*
     * method untuk memeriksa konsistensi matrisk perbandingan berpasangan sebelum disimpan
     */
    private void konsistensiCheck() 
    {
        BOBOTANP anp = new BOBOTANP();
        double [][] initMatriks = new double[kriterias.size()][kriterias.size()];
        double cr;
        
        for(int row = 0; row < kriterias.size(); row++)
        {
            for(int col = 1; col < kriterias.size()+1; col++)
            {
                initMatriks[row][col-1] = Double.parseDouble(tabelMatriks.getValueAt(row, col)+"");
            }
        }
        anp.setNilaiKepentinganKriteria(initMatriks);
        anp.calculatePairwiseComparisonKriteria();
        CalculateUtil.showArray(initMatriks);
        
        lambdaMaxLabel.setText(CalculateUtil.round(anp.getLambdaMax(), 4)+"");
        cILabel.setText(CalculateUtil.round(anp.getCI(),4)+"");
        cRLabel.setText(CalculateUtil.round(anp.getCR(),4)+"");

        if(anp.getCR()>0.1)
        {
            konsistensiLabel.setText("TIDAK KONSISTEN");
        }
        else
        {
            konsistensiLabel.setText("KONSISTEN");
        }
    }
    
    /*
     * method untuk mengatur data pada kriteria dipengaruhi combo sesuai dengan berkas yang dipilih
     */
    public void reModelComboBerkas() 
    {

        kriterias = controllerPenilaianRelasiKriteria.findByBerkasKriteria(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()));
        
        if (kriterias == null || kriterias.isEmpty()) 
        {
            kDipengaruhiCombo.setEnabled(false);
            kriteria1Combo.setEnabled(false);
            kriteria2Combo.setEnabled(false);
            nilaiCombo.setEnabled(false);
            tambahButton.setEnabled(false);
            isSaveNew = true;
        } 
        else 
        {
            kDipengaruhiCombo.setEnabled(true);
            
            kDipengaruhiCombo.setModel(new DefaultComboBoxModel(controllerPenilaianRelasiKriteria.findKriteriaDipengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()))));
            kDipengaruhiCombo.setSelectedIndex(-1);
            isSaveNew = false;
        }
        
    }
    
    /*
     * method untuk mengatur item kriteria pada combo kriteria mempengaruhi sesuai dengan item kriteria yang dipilih pada combo kriteria dipengaruhi
     */
    public void reModelComboKDipengaruhi()
    {
        kriterias = controllerPenilaianRelasiKriteria.findByKriteriaDipengaruhi(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()),controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
        
         if (kriterias == null || kriterias.isEmpty()) 
        {
            kriteria1Combo.setEnabled(false);
            kriteria2Combo.setEnabled(false);
            nilaiCombo.setEnabled(false);
            tambahButton.setEnabled(false);
            isSaveNew = true;
        } 
        else 
        {
            kriteria1Combo.setEnabled(true);
            kriteria2Combo.setEnabled(true);
            nilaiCombo.setEnabled(true);
            tambahButton.setEnabled(true);
            
            kriteria1Combo.setModel(new DefaultComboBoxModel(controllerPenilaianRelasiKriteria.findNamaKriteriaMempengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()),controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()))));
            kriteria1Combo.setSelectedIndex(-1);
            
            kriteria2Combo.setModel(new DefaultComboBoxModel(controllerPenilaianRelasiKriteria.findNamaKriteriaMempengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()),controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()))));
            kriteria2Combo.setSelectedIndex(-1);
            
            isSaveNew = false;
        }
    
    }
    
    /*
     * method untuk mengatur nilai2 perbandingan kepentingan antar kriteria pada tabel matriks
     */
    public void reModelMatriks()
    {
        penilaianRelasiKriterias = controllerPenilaianRelasiKriteria.findByBerkasKriteriaDipengaruhi(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
        matriksPerbandinganPengaruhKriterias = controller.findByBerkasKTDipengaruhi(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
        Object [][] datas = controller.findByBerkasKTDipengaruhiAll(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()),controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
        CostumTableModel tableModelNilai = null;
        
//        System.out.println("Jumlah Data " + matriksPerbandinganPengaruhKriterias.size() + " dengan berkas ukuran index "+ berkasCombo.getSelectedIndex()+ "dan ktdipeng index "+  kDipengaruhiCombo.getSelectedIndex());
//        System.out.println("jumlah data penilaian kriteria "+ penilaianRelasiKriterias.size());
        if(matriksPerbandinganPengaruhKriterias==null || matriksPerbandinganPengaruhKriterias.isEmpty())
        {
            String[] kriteriaArray;
            Object[][] datass;
            kriteriaArray = controllerPenilaianRelasiKriteria.findKriteriaMempengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
            datass = new Object[kriteriaArray.length][kriteriaArray.length+1];
            columnNames = new String[kriteriaArray.length+1];
            columnNames[0] = "Kriteria";
            for(int row=1;row<=kriteriaArray.length;row++)
            {
                columnNames[row]=kriteriaArray[row-1];
            }
            
            for(int row=0;row<datass.length;row++)
            {
                datass[row][0] = kriteriaArray[row];
                for(int col=1;col <= datass.length;col++)
                {
                    if((row-col)==-1)
                    {
                        datass[row][col] = 1;
                    }
                    else
                    {
                        datass[row][col] = 0;
                    }
                }
            }
            tableModelNilai = new CostumTableModel(datass, columnNames);
            isSaveNew = true;

        }
        
        else
        {
            String[] kriteriaArray;
            Object[][] datass;
            int tempIndex=0;
            kriteriaArray = controllerPenilaianRelasiKriteria.findKriteriaMempengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
//            System.out.println("Jumlah Kriteria Mempengaruhi "+ kriteriaArray.length);
            datass = new Object[kriteriaArray.length][kriteriaArray.length+1];
            columnNames = new String[kriteriaArray.length+1];
            columnNames[0] = "Kriteria";
            for(int row=1;row<=kriteriaArray.length;row++)
            {
                columnNames[row]=kriteriaArray[row-1];
            }
            /*
             * untuk nampilinin nilainya
             */
            System.out.println("------------------");
            System.out.println("Jumlah data yang diambil : "+ datas.length);
            for(int i = 0; i<datas.length;i++)
            {
                System.out.println("nilai : "+ datas[i][3]);
            }
            System.out.println("------------------");
            
            
            for(int row=0;row<datass.length;row++)
            {
                datass[row][0] = kriteriaArray[row];
                for(int col=1;col <= datass.length;col++)
                {
                    datass[row][col] = datas[tempIndex][3];
                    tempIndex=tempIndex+1;
                }
            }
            tableModelNilai = new CostumTableModel(datass, columnNames);
            isSaveNew = false;
            
            /*
              * untuk mengedit nilai pada setiap kolom
              */
            boolean[] canEdit = new boolean[kriteriaArray.length+1];
            canEdit[0]=false;
            for(int i = 1; i<kriteriaArray.length+1;i++)
            {
                canEdit[i]=true;
            }
        
            tableModelNilai.setCanEdit(canEdit);
        }
        tabelMatriks.setModel(tableModelNilai);
        
    }
    
    public boolean isSave() 
    {
        return save;
    }

    public void packData() 
    {
        if (isSaveNew) {
           
            matriksPerbandinganPengaruhKriterias = new ArrayList<MatriksPerbandinganPengaruhKriteria>();
            String[] kriteriaArray;
            kriteriaArray = controllerPenilaianRelasiKriteria.findKriteriaMempengaruhiAllArray(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
            
            for (int row = 0; row < kriteriaArray.length; row++) 
            {
                for(int col = 1; col < kriteriaArray.length+1; col++)
                {
//                    System.out.println("Nilai Row "+ row +"-- Nilai Col "+col);
                    matriksPerbandinganPengaruhKriteria= new MatriksPerbandinganPengaruhKriteria();
                    matriksPerbandinganPengaruhKriteria.setNilai(Double.parseDouble(tabelMatriks.getValueAt(row, col)+""));
                    matriksPerbandinganPengaruhKriteria.setRelasi1_id(controllerPenilaianRelasiKriteria.findPenilaianRelasiKriteriaMempengaruhiFromTableIndex(row));
                    matriksPerbandinganPengaruhKriteria.setRelasi2_id(controllerPenilaianRelasiKriteria.findPenilaianRelasiKriteriaMempengaruhiFromTableIndex(col-1));
                    matriksPerbandinganPengaruhKriterias.add(matriksPerbandinganPengaruhKriteria);
                }
            }
        } 
        else 
        {
            matriksPerbandinganPengaruhKriterias = controller.getMatriksPerbandinganPengaruhKriterias();

            double [] nilai = new double[matriksPerbandinganPengaruhKriterias.size()];
            int tempIndex = 0;
            
            for (int row = 0; row < kriterias.size(); row++) 
            {
                for(int col = 1; col < kriterias.size()+1; col++)
                {
                    nilai[tempIndex]= Double.valueOf(tabelMatriks.getValueAt(row, col) + "");
                    tempIndex++;
                }
            }
            
            for(int i = 0; i < matriksPerbandinganPengaruhKriterias.size(); i++)
            {
                matriksPerbandinganPengaruhKriterias.get(i).setNilai(nilai[i]);
            }
             
        }

    }

     public void unpackData() 
    {
        if (matriksPerbandinganPengaruhKriteria == null) 
        {
            return;
        }
 
        kriteria1Combo.setSelectedItem(matriksPerbandinganPengaruhKriteria.getRelasi1_id().getKriteriaM().getNama());
        kriteria2Combo.setSelectedItem(matriksPerbandinganPengaruhKriteria.getRelasi2_id().getKriteriaM().getNama());
        nilaiCombo.setSelectedItem(matriksPerbandinganPengaruhKriteria.getNilai());

    }
    
    public void save()
    {
        packData();
        if(isSaveNew)
        {
            controller.save(matriksPerbandinganPengaruhKriterias);
            JOptionPane.showMessageDialog(null, "data telah disimpan", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            controller.update(matriksPerbandinganPengaruhKriterias);
             JOptionPane.showMessageDialog(null, "data telah diedit", "Informasi", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    public void delete() 
    {
        controller.delete(controllerPenilaianRelasiKriteria.findBerkasFromComboIndex(berkasCombo.getSelectedIndex()), controllerPenilaianRelasiKriteria.findKriteriaFromComboIndex(kDipengaruhiCombo.getSelectedIndex()));
 
        reModelMatriks();
    }

    
}
