/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Penyedia;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author LENOVO
 */
public interface PenyediaDao 
{
     public void save(Penyedia penyedia) throws HibernateException;

    public void update(Penyedia penyedia) throws HibernateException;

    public void delete(Penyedia penyedia) throws HibernateException;

    List<Penyedia> findAll() throws HibernateException;
}
