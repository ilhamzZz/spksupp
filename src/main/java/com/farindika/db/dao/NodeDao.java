/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Node;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author LENOVO
 */
public interface NodeDao 
{
      public void save(Node node) throws HibernateException;

    public void update(Node node) throws HibernateException;

    public void delete(Node node) throws HibernateException;

    List<Node> findAll() throws HibernateException;
}
