/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Berkas;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author LENOVO
 */
public interface BerkasDao 
{
    public void save(Berkas berkas) throws HibernateException;

    public void update(Berkas berkas) throws HibernateException;

    public void delete(Berkas berkas) throws HibernateException;

    List<Berkas> findAll() throws HibernateException;
}
