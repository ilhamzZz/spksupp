/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Pengguna;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Latief
 */
public interface PenggunaDao {

    public void save(Pengguna pengguna) throws HibernateException;

    public void update(Pengguna pengguna) throws HibernateException;

    public void delete(Pengguna pengguna) throws HibernateException;

    List<Pengguna> findAll() throws HibernateException;
    
    Pengguna findPenggunaLogin(String userName, String Password) throws HibernateException;
}
