/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Klaster;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author LENOVO
 */
public interface KlasterDao 
{
    public void save(Klaster klaster) throws HibernateException;

    public void update(Klaster klaster) throws HibernateException;

    public void delete(Klaster klaster) throws HibernateException;

    List<Klaster> findAll() throws HibernateException;
    
    List<Klaster> findByBerkas(Berkas berkas) throws HibernateException;
}
