/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;


import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganPengaruhKriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Ultra
 */
public interface MatriksPerbandinganPengaruhKriteriaDao 
{
    public void save(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException;
    
    public void save(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias) throws HibernateException;

    public void update(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException;
    
    public void update(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias)throws HibernateException;

    public void delete(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException;
    
    public void delete(Berkas berkas, Kriteria kriteria) throws HibernateException;

    List<MatriksPerbandinganPengaruhKriteria> findAll() throws HibernateException;
    
    List<MatriksPerbandinganPengaruhKriteria> findByBerkasKTDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException;
    
    List<Kriteria> findKriteriaByBerkasKTDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException;
    
    List<PenilaianRelasiKriteria> findByBerkas(Berkas berkas) throws HibernateException;
}
