/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;


import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganKriteria;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Ultra
 */
public interface MatriksPerbandinganKriteriaDao 
{
    public void save(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException;
    
    public void save(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) throws HibernateException;

    public void update(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException;
    
    public void update(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias)throws HibernateException;

    public void delete(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException;
    
    public void delete(Berkas berkas) throws HibernateException;

    List<MatriksPerbandinganKriteria> findAll() throws HibernateException;
    
    List<MatriksPerbandinganKriteria> findByBerkas(Berkas berkas) throws HibernateException;
    
    List<Kriteria> findKriteriaByBerkas(Berkas berkas) throws HibernateException;
    
}
