/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;


import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Ultra
 */
public interface PenilaianRelasiKriteriaDao 
{
     public void save(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException;

    public void update(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException;

    public void delete(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException;

    List<PenilaianRelasiKriteria> findAll() throws HibernateException;
    
    List<Kriteria> findByBerkasKriteria(Berkas berkas) throws HibernateException;
    
    List<PenilaianRelasiKriteria> findByBerkas(Berkas berkas) throws HibernateException;
    
    List<Berkas> findByBerkasDistinct(Berkas berkas)throws HibernateException;
    
    List<Kriteria> findByKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException;
    
    List<PenilaianRelasiKriteria> findRelasiKriteria(Berkas berkas, Kriteria kriteria1, Kriteria kriteria2) throws HibernateException;

    List<PenilaianRelasiKriteria> findByBerkasKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException;
}
