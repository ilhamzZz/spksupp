/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.KlasterDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Klaster;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.SetSimpleValueTypeSecondPass;

/**
 *
 * @author LENOVO
 */
public class KlasterDaoImpl implements KlasterDao
{
    private SessionFactory sessionFactory;

    public KlasterDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Klaster klaster) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(klaster);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(Klaster klaster) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(klaster);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(Klaster klaster) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(klaster);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<Klaster> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Klaster> results = (List<Klaster>) session.createQuery("select o from Klaster o").list();
        session.close();
        return results;
    }

    public List<Klaster> findByBerkas(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Klaster> results = (List<Klaster>) session.createQuery("select o from Klaster o  where o.berkas = :berkas")
                .setParameter("berkas", berkas).list();
        session.close();
        return results;
    }
}
