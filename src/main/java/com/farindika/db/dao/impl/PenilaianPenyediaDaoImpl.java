/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.PenilaianPenyediaDao;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianPenyedia;
import com.farindika.db.entity.Penyedia;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ultra
 */
public class PenilaianPenyediaDaoImpl implements PenilaianPenyediaDao {

    private SessionFactory sessionFactory;

    public PenilaianPenyediaDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(PenilaianPenyedia penilaianPenyedia) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
//            penilaianPenyedia.setTanggalPenilaianPenyedia(new GregorianCalendar());
//            penilaianPenyedia.setTanggalUpdate(new GregorianCalendar());
            session.save(penilaianPenyedia);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void save(List<PenilaianPenyedia> penilaianPenyedias) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (PenilaianPenyedia penilaianPenyedia : penilaianPenyedias) {
                session.save(penilaianPenyedia);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(PenilaianPenyedia penilaianPenyedia) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(penilaianPenyedia);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(List<PenilaianPenyedia> penilaianPenyedias) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (PenilaianPenyedia penilaianPenyedia : penilaianPenyedias) {

                PenilaianPenyedia oldPenilaianPenyedia = (PenilaianPenyedia) session.get(PenilaianPenyedia.class, penilaianPenyedia.getId());
                oldPenilaianPenyedia.setKriteria(penilaianPenyedia.getKriteria());
                oldPenilaianPenyedia.setNilai(penilaianPenyedia.getNilai());
                oldPenilaianPenyedia.setPenyedia(penilaianPenyedia.getPenyedia());
                session.update(oldPenilaianPenyedia);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
//            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(PenilaianPenyedia penilaianPenyedia) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(penilaianPenyedia);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void delete(Penyedia penyedia) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.createQuery("delete from PenilaianPenyedia o where o.penyedia=:penyedia").setParameter("penyedia", penyedia).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<PenilaianPenyedia> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianPenyedia> results = (List<PenilaianPenyedia>) session.createQuery("select o from PenilaianPenyedia o").list();
        session.close();
        return results;
    }

    public List<PenilaianPenyedia> findByPenyedia(Penyedia penyedia) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianPenyedia> results = (List<PenilaianPenyedia>) session.createQuery("select o from PenilaianPenyedia o where o.penyedia=:penyedia").setParameter("penyedia", penyedia).list();
        session.close();
        return results;
    }

    
}
