/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.BerkasDao;
import com.farindika.db.entity.Berkas;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author LENOVO
 */
public class BerkasDaoImpl implements BerkasDao 
{
    private SessionFactory sessionFactory;

    public BerkasDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            berkas.setTanggalBerkas(new GregorianCalendar());
            berkas.setTanggalUpdate(new GregorianCalendar());
            session.save(berkas);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            berkas.setTanggalUpdate(new GregorianCalendar());
            session.update(berkas);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(berkas);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<Berkas> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Berkas> results = (List<Berkas>) session.createQuery("select o from Berkas o").list();
        session.close();
        return results;
    }
}
