/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.JabatanDao;
import com.farindika.db.entity.Jabatan;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Latief
 */
public class JabatanDaoImpl implements JabatanDao
{

    private SessionFactory sessionFactory;

    public JabatanDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Jabatan jabatan) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(jabatan);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(Jabatan jabatan) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(jabatan);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(Jabatan jabatan) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(jabatan);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<Jabatan> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Jabatan> results = (List<Jabatan>) session.createQuery("select o from Jabatan o").list();
        session.close();
        return results;
    }
}
