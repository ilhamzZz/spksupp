/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.PenggunaDao;
import com.farindika.db.entity.Pengguna;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Latief
 */
public class PenggunaDaoImpl implements PenggunaDao {

    private SessionFactory sessionFactory;

    public PenggunaDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Pengguna pengguna) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(pengguna);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(Pengguna pengguna) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(pengguna);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(Pengguna pengguna) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(pengguna);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<Pengguna> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Pengguna> results = (List<Pengguna>) session.createQuery("select o from Pengguna o").list();
        session.close();
        return results;
    }

    public Pengguna findPenggunaLogin(String userName, String password) throws HibernateException {
        Session session = sessionFactory.openSession();
        Pengguna results = (Pengguna) session.createQuery("select o from Pengguna o where o.userName=:userName and o.password=:password").setParameter("userName", userName).setParameter("password", password).uniqueResult();
        session.close();
        return results;
    }
}
