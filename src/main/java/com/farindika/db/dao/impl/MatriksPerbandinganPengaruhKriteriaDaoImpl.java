/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.MatriksPerbandinganPengaruhKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganPengaruhKriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ultra
 */
public class MatriksPerbandinganPengaruhKriteriaDaoImpl implements MatriksPerbandinganPengaruhKriteriaDao 
{
    private SessionFactory sessionFactory;

    public MatriksPerbandinganPengaruhKriteriaDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(matriksPerbandinganPengaruhKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void save(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria : matriksPerbandinganPengaruhKriterias) {
                session.save(matriksPerbandinganPengaruhKriteria);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(matriksPerbandinganPengaruhKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void update(List<MatriksPerbandinganPengaruhKriteria> matriksPerbandinganPengaruhKriterias) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria : matriksPerbandinganPengaruhKriterias) {

                MatriksPerbandinganPengaruhKriteria oldMatriksPerbandinganPengaruhKriteria = (MatriksPerbandinganPengaruhKriteria) session.get(MatriksPerbandinganPengaruhKriteria.class, matriksPerbandinganPengaruhKriteria.getId());
                oldMatriksPerbandinganPengaruhKriteria.setRelasi1_id(matriksPerbandinganPengaruhKriteria.getRelasi1_id());
                oldMatriksPerbandinganPengaruhKriteria.setRelasi2_id(matriksPerbandinganPengaruhKriteria.getRelasi2_id());
                oldMatriksPerbandinganPengaruhKriteria.setNilai(matriksPerbandinganPengaruhKriteria.getNilai());
                
                session.update(oldMatriksPerbandinganPengaruhKriteria);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
//            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(MatriksPerbandinganPengaruhKriteria matriksPerbandinganPengaruhKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(matriksPerbandinganPengaruhKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
   
    public void delete(Berkas berkas, Kriteria kriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.createQuery("delete from MatriksPerbandinganPengaruhKriteria o "
                    + "where o.id in "
                    + "     (select m.id from MatriksPerbandinganPengaruhKriteria m where m.relasi1_id.berkas=:berkas and m.relasi1_id.kriteriaD=:kriteria )"
                    + "")
                    .setParameter("berkas", berkas).setParameter("kriteria", kriteria).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<MatriksPerbandinganPengaruhKriteria> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<MatriksPerbandinganPengaruhKriteria> results = (List<MatriksPerbandinganPengaruhKriteria>) session.createQuery("select o from MatriksPerbandinganPengaruhKriteria o").list();
        session.close();
        return results;
    }

    public List<PenilaianRelasiKriteria> findByBerkas(Berkas berkas) {
        Session session = sessionFactory.openSession();
        List<PenilaianRelasiKriteria> results = (List<PenilaianRelasiKriteria>) session.createQuery("select distinct o.berkas from PenilaianRelasiKriteria o where o.berkas=:berkas").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

    public List<MatriksPerbandinganPengaruhKriteria> findByBerkasKTDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<MatriksPerbandinganPengaruhKriteria> results = (List<MatriksPerbandinganPengaruhKriteria>) session.createQuery(""
                + "select o from MatriksPerbandinganPengaruhKriteria o "
                + "where o.relasi1_id.berkas=:berkas and o.relasi1_id.kriteriaD=:kriteria")
                .setParameter("berkas", berkas).setParameter("kriteria", kriteria).list();
        session.close();
        return results;
    }

    public List<Kriteria> findKriteriaByBerkasKTDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Kriteria> results = (List<Kriteria>) session.createQuery(""
                + "select o from MatriksPerbandinganPengaruhKriteria o "
                + "where o.relasi1_id.berkas=:berkas and o.relasi1_id.kriteriaD=:kriteria")
                .setParameter("berkas", berkas).setParameter("kriteria", kriteria).list();
        session.close();
        return results;
    }
}
