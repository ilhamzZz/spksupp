/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.PenilaianRelasiKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianRelasiKriteria;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ultra
 */
public class PenilaianRelasiKriteriaDaoImpl implements PenilaianRelasiKriteriaDao
{
    private SessionFactory sessionFactory;

    public PenilaianRelasiKriteriaDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(penilaianRelasiKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(penilaianRelasiKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(PenilaianRelasiKriteria penilaianRelasiKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(penilaianRelasiKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<PenilaianRelasiKriteria> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianRelasiKriteria> results = (List<PenilaianRelasiKriteria>) session.createQuery("select o from PenilaianRelasiKriteria o").list();
        session.close();
        return results;
    }

    public List<Kriteria> findByBerkasKriteria(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Kriteria> results = (List<Kriteria>) session.createQuery("select distinct kd from PenilaianRelasiKriteria p join p.kriteriaD kd where p.berkas =:berkas group by p.kriteriaD").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

    public List<PenilaianRelasiKriteria> findByBerkas(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianRelasiKriteria> results = (List<PenilaianRelasiKriteria>) session.createQuery("select o from PenilaianRelasiKriteria o where o.berkas=:berkas").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

    public List<Kriteria> findByKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Kriteria> results = (List<Kriteria>) session.createQuery("select distinct km from PenilaianRelasiKriteria p join p.kriteriaM km where p.berkas =:berkas and p.kriteriaD =:kriteria group by p.kriteriaM").setParameter("berkas", berkas).setParameter("kriteria", kriteria).list();
        session.close();
        return results;
    }

    public List<PenilaianRelasiKriteria> findRelasiKriteria(Berkas berkas, Kriteria kriteria1, Kriteria kriteria2) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianRelasiKriteria> results = (List<PenilaianRelasiKriteria>) session.createQuery("select id from PenilaianRelasiKriteria p join p.id id where p.berkas =:berkas and p.kriteriaD =:kriteria1 and p.kriteriaM =:kriteria2 group by p.kriteriaM").setParameter("berkas", berkas).setParameter("kriteria1", kriteria1).setParameter("kriteria2", kriteria2).list();
        session.close();
        return results;
    }

    public List<Berkas> findByBerkasDistinct(Berkas berkas) throws HibernateException {
         Session session = sessionFactory.openSession();
        List<Berkas> results = (List<Berkas>) session.createQuery("select distinct b from PenilaianRelasiKriteria p join p.berkas b where p.berkas =:berkas group by p.berkas").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

    public List<PenilaianRelasiKriteria> findByBerkasKriteriaDipengaruhi(Berkas berkas, Kriteria kriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<PenilaianRelasiKriteria> results = (List<PenilaianRelasiKriteria>) session.createQuery("select o from PenilaianRelasiKriteria o where o.berkas =:berkas and o.kriteriaD =:kriteria").setParameter("berkas", berkas).setParameter("kriteria", kriteria).list();
        session.close();
        return results;

    }
}
