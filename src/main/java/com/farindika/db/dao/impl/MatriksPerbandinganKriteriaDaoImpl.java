/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao.impl;

import com.farindika.db.dao.MatriksPerbandinganKriteriaDao;
import com.farindika.db.entity.Berkas;
import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.MatriksPerbandinganKriteria;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Ultra
 */
public class MatriksPerbandinganKriteriaDaoImpl implements MatriksPerbandinganKriteriaDao 
{
    private SessionFactory sessionFactory;

    public MatriksPerbandinganKriteriaDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(matriksPerbandinganKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void save(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) throws HibernateException {
         Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (MatriksPerbandinganKriteria matriksPerbandinganKriteria : matriksPerbandinganKriterias) {
                session.save(matriksPerbandinganKriteria);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void update(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(matriksPerbandinganKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void update(List<MatriksPerbandinganKriteria> matriksPerbandinganKriterias) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            for (MatriksPerbandinganKriteria matriksPerbandinganKriteria : matriksPerbandinganKriterias) {

                MatriksPerbandinganKriteria oldMatriksPerbandinganKriteria = (MatriksPerbandinganKriteria) session.get(MatriksPerbandinganKriteria.class, matriksPerbandinganKriteria.getId());
                oldMatriksPerbandinganKriteria.setBerkas(matriksPerbandinganKriteria.getBerkas());
                oldMatriksPerbandinganKriteria.setKriteria1(matriksPerbandinganKriteria.getKriteria1());
                oldMatriksPerbandinganKriteria.setKriteria2(matriksPerbandinganKriteria.getKriteria2());
                oldMatriksPerbandinganKriteria.setNilai(matriksPerbandinganKriteria.getNilai());
                session.update(oldMatriksPerbandinganKriteria);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
//            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public void delete(MatriksPerbandinganKriteria matriksPerbandinganKriteria) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(matriksPerbandinganKriteria);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }
    
    public void delete(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.createQuery("delete from MatriksPerbandinganKriteria o where o.berkas=:berkas").setParameter("berkas", berkas).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new HibernateException(e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<MatriksPerbandinganKriteria> findAll() throws HibernateException {
        Session session = sessionFactory.openSession();
        List<MatriksPerbandinganKriteria> results = (List<MatriksPerbandinganKriteria>) session.createQuery("select o from MatriksPerbandinganKriteria o").list();
        session.close();
        return results;
    }

    public List<MatriksPerbandinganKriteria> findByBerkas(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<MatriksPerbandinganKriteria> results = (List<MatriksPerbandinganKriteria>) session.createQuery("select o from MatriksPerbandinganKriteria o where o.berkas=:berkas").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

    public List<Kriteria> findKriteriaByBerkas(Berkas berkas) throws HibernateException {
        Session session = sessionFactory.openSession();
        List<Kriteria> results = (List<Kriteria>) session.createQuery("select distinct k from MatriksPerbandinganKriteria o join o.kriteria1 k where o.berkas=:berkas").setParameter("berkas", berkas).list();
        session.close();
        return results;
    }

           
}
