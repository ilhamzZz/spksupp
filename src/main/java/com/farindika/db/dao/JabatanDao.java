/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Jabatan;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Latief
 */
public interface JabatanDao {

    public void save(Jabatan jabatan) throws HibernateException;

    public void update(Jabatan jabatan) throws HibernateException;

    public void delete(Jabatan jabatan) throws HibernateException;

    List<Jabatan> findAll() throws HibernateException;
}
