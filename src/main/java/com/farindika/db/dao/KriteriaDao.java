/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.Penyedia;
import java.util.List;
import org.hibernate.HibernateException;
/**
 *
 * @author Ultra
 */
public interface KriteriaDao 
{
     public void save(Kriteria kriteria) throws HibernateException;

    public void update(Kriteria kriteria) throws HibernateException;

    public void delete(Kriteria kriteria) throws HibernateException;

    List<Kriteria> findAll() throws HibernateException;
   
}
