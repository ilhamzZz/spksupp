/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.dao;

import com.farindika.db.entity.Kriteria;
import com.farindika.db.entity.PenilaianPenyedia;
import com.farindika.db.entity.Penyedia;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Ultra
 */
public interface PenilaianPenyediaDao 
{
    public void save(PenilaianPenyedia penilaianPenyedia) throws HibernateException;
    
    public void save(List<PenilaianPenyedia> penilaianPenyedia) throws HibernateException;

    public void update(PenilaianPenyedia penilaianPenyedia) throws HibernateException;
    
    public void update(List<PenilaianPenyedia> penilaianPenyedia)throws HibernateException;

    public void delete(PenilaianPenyedia penilaianPenyedia) throws HibernateException;
    
    public void delete(Penyedia penyedia) throws HibernateException;

    List<PenilaianPenyedia> findAll() throws HibernateException;
    
    List<PenilaianPenyedia> findByPenyedia(Penyedia penyedia) throws HibernateException;
}
