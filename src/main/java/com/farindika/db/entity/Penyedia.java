/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.util.Calendar;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "penyedia")
public class Penyedia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "penyedia_id", nullable = false)
    private int id;
    @Column(name = "nama_penyedia", nullable = false, length = 50)
    private String nama;
    @Column(name = "alamat", nullable = false, length = 50)
    private String alamat;
    @Column(name = "telepon", nullable = false, length = 50)
    private String telp;
    @Column(name = "email", nullable = true, length = 50)
    private String email;
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_daftar")
    private Calendar tanggalDaftar;
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_update")
    private Calendar tanggalUpdate;
    @Column(name = "keterangan", nullable = true, length = 100)
    private String keterangan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public Calendar getTanggalDaftar() {
        return tanggalDaftar;
    }

    public void setTanggalDaftar(Calendar tanggalDaftar) {
        this.tanggalDaftar = tanggalDaftar;
    }

    public Calendar getTanggalUpdate() {
        return tanggalUpdate;
    }

    public void setTanggalUpdate(Calendar tanggalUpdate) {
        this.tanggalUpdate = tanggalUpdate;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
