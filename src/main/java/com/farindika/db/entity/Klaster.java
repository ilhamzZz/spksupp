/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "klaster")
public class Klaster implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "klaster_id", nullable = false)
    private int id;
    @Column(name = "kode_klaster", length = 50, nullable = false)
    private String kode;
    @Column(name = "nama_klaster", length = 50, nullable = false)
    private String nama;
    @ManyToOne
    @JoinColumn(name = "berkas_id", nullable = true)
    private Berkas berkas;
    @Column(name = "keterangan", length = 100, nullable = true)
    private String keterangan;
    @Column(name = "alternatif", nullable = true)
    private boolean alternatif;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Berkas getBerkas() {
        return berkas;
    }

    public void setBerkas(Berkas berkas) {
        this.berkas = berkas;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public boolean isAlternatif() {
        return alternatif;
    }

    public void setAlternatif(boolean alternatif) {
        this.alternatif = alternatif;
    }
}
