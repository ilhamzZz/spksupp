/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Ultra
 */

@Entity
@Table(name = "penilaianrelasikriteria")
public class PenilaianRelasiKriteria implements Serializable
{   @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "relasi_kriteria_id", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "kriteria_dipengaruhi_id", nullable = true)
    private Kriteria kriteriaD;
    
    @ManyToOne
    @JoinColumn(name = "kriteria_mempengaruhi_id", nullable = true)
    private Kriteria kriteriaM;
    
    @ManyToOne
    @JoinColumn(name = "berkas_id", nullable = true)
    private Berkas berkas;

    public Berkas getBerkas() {
        return berkas;
    }

    public void setBerkas(Berkas berkas) {
        this.berkas = berkas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kriteria getKriteriaD() {
        return kriteriaD;
    }

    public void setKriteriaD(Kriteria kriteriaD) {
        this.kriteriaD = kriteriaD;
    }

    public Kriteria getKriteriaM() {
        return kriteriaM;
    }

    public void setKriteriaM(Kriteria kriteriaM) {
        this.kriteriaM = kriteriaM;
    }
    
    
}
