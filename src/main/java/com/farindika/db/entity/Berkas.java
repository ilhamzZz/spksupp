/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "berkas")
public class Berkas implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "berkas_id", nullable = false)
    private int id;
    @Column(name = "nama_berkas", length = 50, nullable = false)
    private String nama;
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_berkas", length = 50, nullable = false)
    private Calendar tanggalBerkas;
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_update", length = 50, nullable = false)
    private Calendar tanggalUpdate;
    @ManyToOne
    @JoinColumn(name = "pengguna_id", nullable = false)
    private Pengguna pengguna;
//    @ManyToOne
//    @JoinColumn(name = "pengguna_id", nullable = false)
//    private Pengguna pengguna_akhir;
    @Column(name = "keterangan", length = 100, nullable = true)
    private String keterangan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Calendar getTanggalBerkas() {
        return tanggalBerkas;
    }

    public void setTanggalBerkas(Calendar tanggalBerkas) {
        this.tanggalBerkas = tanggalBerkas;
    }

    public Calendar getTanggalUpdate() {
        return tanggalUpdate;
    }

    public void setTanggalUpdate(Calendar tanggalUpdate) {
        this.tanggalUpdate = tanggalUpdate;
    }

    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

//    public Pengguna getPengguna_akhir() {
//        return pengguna_akhir;
//    }
//
//    public void setPengguna_akhir(Pengguna pengguna_akhir) {
//        this.pengguna_akhir = pengguna_akhir;
//    }
    
}
