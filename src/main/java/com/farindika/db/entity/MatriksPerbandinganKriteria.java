/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Ultra
 */

@Entity
@Table(name = "matriksperbandingankriteria")
public class MatriksPerbandinganKriteria implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "perbandingan_kriteria_id", nullable = false)
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "kriteria1_id", nullable = true)
    private Kriteria kriteria1;
    
    @ManyToOne
    @JoinColumn(name = "kriteria2_id", nullable = true)
    private Kriteria kriteria2;
    
    @Column(name = "nilai", nullable = true)
    private Double nilai;
    
    @ManyToOne
    @JoinColumn(name = "berkas_id", nullable = true)
    private Berkas berkas;

    public Berkas getBerkas() {
        return berkas;
    }

    public void setBerkas(Berkas berkas) {
        this.berkas = berkas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kriteria getKriteria1() {
        return kriteria1;
    }

    public void setKriteria1(Kriteria kriteria1) {
        this.kriteria1 = kriteria1;
    }

    public Kriteria getKriteria2() {
        return kriteria2;
    }

    public void setKriteria2(Kriteria kriteria2) {
        this.kriteria2 = kriteria2;
    }

    public Double getNilai() {
        return nilai;
    }

    public void setNilai(Double nilai) {
        this.nilai = nilai;
    }
    
    
}
