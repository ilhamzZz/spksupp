/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "node")
public class Node implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "node_id", nullable = false)
    private int id;
    @Column(name = "kode_node", length = 50, nullable = false)
    private String kode;
    @Column(name = "nama_node", length = 50, nullable = false)
    private String nama;
    @Column(name = "keterangan", length = 100, nullable = true)
    private String keterangan;
    @ManyToOne
    @JoinColumn(name = "klaster_id", nullable = true)
    private Klaster klaster;
 
    @ManyToOne
    @JoinColumn(name = "penyedia_id", nullable = true)
    private Penyedia penyedia;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Klaster getKlaster() {
        return klaster;
    }

    public void setKlaster(Klaster klaster) {
        this.klaster = klaster;
    }
    
      public Penyedia getPenyedia() {
        return penyedia;
    }

    public void setPenyedia(Penyedia penyedia) {
        this.penyedia = penyedia;
    }
}
