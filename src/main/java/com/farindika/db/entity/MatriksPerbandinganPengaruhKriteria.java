/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Ultra
 */

@Entity
@Table(name = "matriksperbandinganpengaruhkriteria")
public class MatriksPerbandinganPengaruhKriteria implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "perbandingan_pengaruh_id", nullable = false)
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "relasi1_id", nullable = true)
    private PenilaianRelasiKriteria relasi1_id;
    
    @ManyToOne
    @JoinColumn(name = "relasi2_id", nullable = true)
    private PenilaianRelasiKriteria relasi2_id;
    
    @Column(name = "nilai", nullable = true)
    private Double nilai;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getNilai() {
        return nilai;
    }

    public void setNilai(Double nilai) {
        this.nilai = nilai;
    }

    public PenilaianRelasiKriteria getRelasi1_id() {
        return relasi1_id;
    }

    public void setRelasi1_id(PenilaianRelasiKriteria relasi1_id) {
        this.relasi1_id = relasi1_id;
    }

    public PenilaianRelasiKriteria getRelasi2_id() {
        return relasi2_id;
    }

    public void setRelasi2_id(PenilaianRelasiKriteria relasi2_id) {
        this.relasi2_id = relasi2_id;
    }
    
    
}
