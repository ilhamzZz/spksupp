/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Ultra
 */

@Entity
@Table(name = "penilaianpenyedia")
public class PenilaianPenyedia implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "penilaian_id", nullable = false)
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "penyedia_id", nullable = true)
    private Penyedia penyedia;
    
    @ManyToOne
    @JoinColumn(name = "kriteria_id", nullable = true)
    private Kriteria kriteria;
    
    @Column(name = "nilai", nullable = true)
    private Double nilai;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kriteria getKriteria() {
        return kriteria;
    }

    public void setKriteria(Kriteria kriteria) {
        this.kriteria = kriteria;
    }

    public Double getNilai() {
        return nilai;
    }

    public void setNilai(Double nilai) {
        this.nilai = nilai;
    }

    public Penyedia getPenyedia() {
        return penyedia;
    }

    public void setPenyedia(Penyedia penyedia) {
        this.penyedia = penyedia;
    }
    
    
}
