/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farindika.db;

import com.farindika.db.dao.*;
import com.farindika.db.dao.impl.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Latief
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static JabatanDao jabatanDao;
    private static PenggunaDao penggunaDao;
    private static PenyediaDao penyediaDao;
    private static KlasterDao klasterDao;
    private static NodeDao nodeDao;
    private static BerkasDao berkasDao;
    private static KriteriaDao kriteriaDao;
    private static PenilaianPenyediaDao penilaianPenyediaDao;
    private static MatriksPerbandinganKriteriaDao matriksPerbandinganKriteriaDao;
    private static PenilaianRelasiKriteriaDao penilaianRelasiKriteriaDao;
    private static MatriksPerbandinganPengaruhKriteriaDao matriksPerbandinganPengaruhKriteriaDao;
    
    static {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static JabatanDao getJabatanDao(){
        if(jabatanDao == null){
            jabatanDao = new JabatanDaoImpl(sessionFactory);
        }
        return jabatanDao;
    }
    
    public static PenggunaDao getPenggunaDao(){
        if(penggunaDao == null){
            penggunaDao = new PenggunaDaoImpl(sessionFactory);
        }
        return penggunaDao;
    }
    public static PenyediaDao getPenyediaDao(){
        if(penyediaDao == null){
            penyediaDao = new PenyediaDaoImpl(sessionFactory);
        }
        return penyediaDao;
    }
     public static KlasterDao getKlasterDao(){
        if(klasterDao == null){
            klasterDao = new KlasterDaoImpl(sessionFactory);
        }
        return klasterDao;
    }
     
     public static NodeDao getNodeDao(){
        if(nodeDao == null){
            nodeDao = new NodeDaoImpl(sessionFactory);
        }
        return nodeDao;
    }
     
      public static BerkasDao getBerkasDao(){
        if(berkasDao == null){
            berkasDao = new BerkasDaoImpl(sessionFactory);
        }
        return berkasDao;
    }
      
     public static KriteriaDao getKriteriaDao(){
        if(kriteriaDao == null){
            kriteriaDao = new KriteriaDaoImpl(sessionFactory);
        }
        return kriteriaDao;
    }
     
     public static PenilaianPenyediaDao getPenilaianPenyediaDao(){
        if(penilaianPenyediaDao == null){
            penilaianPenyediaDao = new PenilaianPenyediaDaoImpl(sessionFactory);
        }
        return penilaianPenyediaDao;
    }
     
    public static MatriksPerbandinganKriteriaDao getMatriksPerbandinganKriteriaDao(){
        if(matriksPerbandinganKriteriaDao == null){
            matriksPerbandinganKriteriaDao = new MatriksPerbandinganKriteriaDaoImpl(sessionFactory);
        }
        return matriksPerbandinganKriteriaDao;
    }
    
    public static MatriksPerbandinganPengaruhKriteriaDao getMatriksPerbandinganPengaruhKriteriaDao(){
        if(matriksPerbandinganPengaruhKriteriaDao == null){
            matriksPerbandinganPengaruhKriteriaDao = new MatriksPerbandinganPengaruhKriteriaDaoImpl(sessionFactory);
        }
        return matriksPerbandinganPengaruhKriteriaDao;
    }
    
    public static PenilaianRelasiKriteriaDao getPenilaianRelasiKriteriaDao(){
        if(penilaianRelasiKriteriaDao == null){
            penilaianRelasiKriteriaDao = new PenilaianRelasiKriteriaDaoImpl(sessionFactory);
        }
        return penilaianRelasiKriteriaDao;
    }
}
